#include <iostream>
#include <fstream>
#include <stdexcept>
#include <iostream>
#include <cstring>
#include <string>
#include <vector>

#include <execinfo.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>

// #include <chrono>  // for high_resolution_clock

#include "TString.h"
#include "TH2I.h"
#include "TH2D.h"
#include "TH3I.h"
#include "TGraph.h"
#include "TStyle.h"
#include "TCanvas.h"
#include "TLatex.h"
#include "TFile.h"
#include "TROOT.h"
#include "TColor.h"
#include "TSystem.h"
#include "TTree.h"

#include "include/Palette.hh"
#include "include/Options.hh"
#include "include/Conversion.hh"
#include "include/Visualization.hh"
#include "include/Trigger.hh"

TString create_valid_pathname(const TString& outputprefix_base, const TString& outputprefix_filename_base, const TString& suffix, unsigned short max_file_name_length);

inline bool file_exists(const char* pathname) {
  struct stat buffer;   
  return (stat(pathname, &buffer) == 0); 
}

int main(Int_t argc, char** argv) {
//     gSystem->Load("libTree.so");
      
    try {
        Palette palette;
        palette.InvBPalette();
        
        Options options(argc, argv);

        if(options.IsHelp()) {
            Options::PrintHelp();
            return 0;
        }


        Visualization visualization;
        Conversion conversion;
        Trigger trigger(options);

        std::vector<std::string> inputFiles = options.GetInputFiles();
        
        for(std::vector<std::string>::iterator inputFileIt=inputFiles.begin(); inputFileIt!=inputFiles.end(); ++inputFileIt) {
            std::string fname = *inputFileIt;
            TString fname_md5 = TString(fname.c_str()).MD5();
            TString fname_stdout_prefix = "(" + fname_md5 + ") ";
            
            std::cout << fname_stdout_prefix << "Processing file: "  << fname << std::endl;
            
            try {

                Float_t aveBgFactor = options.GetAveBgFactor();
                
                std::size_t lastSlashPos = fname.find_last_of("/\\");
                if(lastSlashPos == std::string::npos) {
                    lastSlashPos = 0;
                } else if(lastSlashPos+1 >= fname.length()) {
                    throw std::runtime_error("Missing file name in the path on input (\"" +  fname + "\")");
                }
                
                std::string::size_type squeeze = fname.find("sqz", lastSlashPos);

                if(squeeze != std::string::npos)
                    squeeze = 1;
                if (squeeze == 1) {
                    aveBgFactor *= Float_t(options.GetPacketSize())/25.0f;   // !!! WARNING changed from 128./24 to 128./25
                }

                TString outputprefix_filename_base;
                TString outputprefix_trigger_filename_base;
                TString outputprefix_dir;
                TString outputprefix_base;
                TString inverseMeansFrame_basename;
                
                if (options.IsInverseMeansFrame()) {
                    inverseMeansFrame_basename = gSystem->BaseName(options.GetInverseMeansFramePathname());
                    Ssiz_t first_dot = inverseMeansFrame_basename.First('.');
                    inverseMeansFrame_basename = inverseMeansFrame_basename(0,first_dot);
                }
                
                outputprefix_dir = options.GetDataDir();
                if(outputprefix_dir[outputprefix_dir.Length()-1] != '/') {
                    outputprefix_dir += "/";
                }
                
                outputprefix_base = "trn_";
                
                if(!options.IsSimplePrefix()) {
                    std::string::size_type aquisition = fname.find("ACQUISITION-", lastSlashPos);
                    if(aquisition == std::string::npos || (!options.IsEcAscInput() && fname.substr(fname.length()-5,5) != ".root")) {
                        throw std::runtime_error(fname + " " + fname_stdout_prefix.Data() + ": not an ACQUISITION root file");
                    }

                    outputprefix_filename_base = fname.substr(aquisition+12,23);
                }
                else {
                    std::string::size_type lastDotPos = fname.find_last_of(".");
                    std::string filenameNoExt;
                    if(lastDotPos != std::string::npos && lastDotPos > lastSlashPos) {
                        filenameNoExt = fname.substr(lastSlashPos+1, lastDotPos-lastSlashPos-1);
                    } else {
                        filenameNoExt = fname.substr(lastSlashPos+1);
                    }
                    
                    outputprefix_filename_base = filenameNoExt.c_str(); 
                }
                
                outputprefix_trigger_filename_base = outputprefix_filename_base;

                if(options.IsInverseMeansFrame()) {
                    outputprefix_trigger_filename_base += "_C_" + inverseMeansFrame_basename;
                }
                outputprefix_trigger_filename_base += "_bgf_" + TString::Format("%.2f",aveBgFactor);
                
                
    //             std::cout << argc << " " << outputprefix << " " << outputprefixShort << " | " << fname <<  " { | | " << argv[1] << std::endl;

                struct stat dirInfo;
                if(stat( options.GetDataDir(), &dirInfo ) != 0) {
                    throw std::runtime_error(TString("Cannot access " + options.GetDataDir()).Data());
                }
                else if(!(dirInfo.st_mode & S_IFDIR)) {
                    throw std::runtime_error(TString("Data dir " + options.GetDataDir() + " is not directory").Data());
                }

                TString triggerInputName;
                unsigned short max_file_name_length = options.GetMaxFileNameLength();

                if(options.IsConvertYes() && !options.IsEcAscInput()) {
                    
                    TString pdm_asc_pathname = outputprefix_dir + create_valid_pathname(outputprefix_base, outputprefix_filename_base, "_pdm_asc.gz", max_file_name_length);
                    TString ec_asc_pathname = outputprefix_dir + create_valid_pathname(outputprefix_base, outputprefix_filename_base, "_ec_asc.gz", max_file_name_length);
                    
                    std::cout << fname_stdout_prefix << "PDM_ASC file: " << pdm_asc_pathname << std::endl;
                    std::cout << fname_stdout_prefix << "EC_ASC file: " << ec_asc_pathname << std::endl;
                    
                    // WARNING pdm_asc_pathname is not checked
                    if (options.IsOverwrite() || !file_exists(ec_asc_pathname)) { 
                        conversion.ConvertFile3(fname.c_str(), pdm_asc_pathname.Data(), ec_asc_pathname.Data());
                        triggerInputName = ec_asc_pathname;
                    }
                    else {
                        std::cout << fname_stdout_prefix << "EC_ASC file already exists, skipping conversion." << std::endl;
                    }
                }
                else {
                    std::cout << fname_stdout_prefix << "Skipping conversion" << std::endl;
                    triggerInputName = fname.c_str();
                }

                if(options.IsDotrigger()) {
                    
                    TString trgrootfile = outputprefix_dir + create_valid_pathname(outputprefix_base, outputprefix_trigger_filename_base, ".root", max_file_name_length);
                    
                    std::cout << fname_stdout_prefix << "Trigger information file: " << trgrootfile << std::endl;

                    if (options.IsOverwrite() || !file_exists(trgrootfile)) { 
                        
                        long file_position = std::distance(inputFiles.begin(), inputFileIt);
                        
                        if(!options.IsConvertYes() && !options.IsEcAscInput()) {
                            TFile *inputFile = TFile::Open(fname.c_str());
                            if (inputFile == 0 || inputFile->IsZombie()) {
                                throw std::runtime_error(TString::Format("Opening input file \"%s\" failed (0 or zombie).",trgrootfile.Data()).Data());
                            }
                            
                            if(file_position == 0 && options.IsReadThresholdsForFirstPacket()) {
                                PhotonCountDataIterator<Int_t> it1(inputFile, false);
                                trigger.ReadThresholdsForFirstPacket(aveBgFactor, options.GetPacketSize(), &it1);
                            }
                            
                            PhotonCountDataIterator<Int_t> it2(inputFile, false);
                            trigger.FenuRootOut(aveBgFactor, options.GetPacketSize(), &it2, trgrootfile, fname_stdout_prefix);
                            
                            inputFile->Close();
                            delete inputFile;
                        }
                        else {
                            TString pfilename = "gzip -dc " + triggerInputName;
                            FILE* file = popen(pfilename, "r");
                            
                            if(file_position == 0 && options.IsReadThresholdsForFirstPacket()) {
                                PhotonCountDataIterator<Int_t> it1(file);
                                trigger.ReadThresholdsForFirstPacket(aveBgFactor, options.GetPacketSize(), &it1);
                            }
                            
                            rewind(file);
                            
                            PhotonCountDataIterator<Int_t> it2(file);
                            trigger.FenuRootOut(aveBgFactor, options.GetPacketSize(), &it2, trgrootfile, fname_stdout_prefix);
                            
                            fclose(file);
                        }
                    }
                    else {
                        std::cout << fname_stdout_prefix << "Trigger root file already exists, skipping triggering." << std::endl;
                    }
                    
                } else {
                    std::cout << fname_stdout_prefix << "Skipping triggering algorithm" << std::endl;
                }

                if(options.IsVisualisation() == true && !options.IsEcAscInput()) {
                    
                    // This might not be correct, needs to be reviewed
                    
                    TString trgrootfile = outputprefix_dir + create_valid_pathname(outputprefix_base, outputprefix_trigger_filename_base, ".root", max_file_name_length);
                    TString outputprefix = outputprefix_dir + create_valid_pathname(outputprefix_base, outputprefix_trigger_filename_base, "", max_file_name_length - 15);
                    
                    visualization.TrigVis7(TString(fname), trgrootfile, outputprefix);
                } else {
                    std::cout << fname_stdout_prefix << "Skipping visualization" << std::endl;
                }
              
            } catch (std::exception & ex) {
                std::cerr << fname_stdout_prefix << "Failure: " << ex.what() << std::endl;
            }    
            
        }

        if(options.IsTriggerVisual()) {
            trigger.CreateHistogramAndGraphForMaxForPMT();

            if(options.IsUseMinAndMaxSignals()) {
                trigger.CreateHistogramForNumberOfFrame();
                trigger.CreateHistogramForThresholds();
                trigger.CreateHistogramForTriggerRate();
            }
        }     
    } catch (std::invalid_argument & ex) {
        std::cerr << "Invalid argument: " << ex.what() << std::endl;
        Options::PrintHelp();
        exit(12);
    } catch (std::exception & ex) {
        std::cerr << "Exception: " << ex.what() << std::endl;
        exit(11);
    }

    return 0;
}


TString create_valid_pathname(const TString& outputprefix_base, const TString& outputprefix_filename_base, const TString& suffix, unsigned short max_file_name_length) {
    const unsigned short MD5_HEXDIGSEST_LEN = 32; 
        
    TString outputrootfile = outputprefix_base + outputprefix_filename_base + suffix;
    
    if (outputrootfile.Length() > max_file_name_length) {
        
        unsigned short i=1;
        for (; i<=4; ++i) {            
            if (outputrootfile.Length() - outputprefix_filename_base.Length()  + MD5_HEXDIGSEST_LEN/i <= max_file_name_length) {
                outputrootfile = outputprefix_base + outputprefix_filename_base.MD5()(0,MD5_HEXDIGSEST_LEN/i) + suffix;
                break;
            }
        }
        if (i == 5) {
            throw std::runtime_error("Cannot sufficiently shorten file path " + outputprefix_base + outputprefix_filename_base + suffix);
        }
    }

    return outputrootfile;
}
