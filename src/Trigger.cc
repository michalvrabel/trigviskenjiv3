/*
 * Trigger.cc
 *
 *  Created on: Feb 25, 2018
 *      Author: Kristián Goliaš
 */

#include "Trigger.hh"

#include <iostream>
#include <fstream>
#include <algorithm>
#include <limits>

#include <math.h>
#include <time.h>
#include <stdio.h>

#include "TFile.h"
#include "TTree.h"
#include "TGraph.h"

// #define MAXGTUINFILE 500000
const Int_t MAXGTUINFILE = 1000000;

Trigger::Trigger(const Options& option) : fOptions(option), fGtuCounter(0) {
    
    // These are defined in the header file:
    // const unsigned short kTRIGGER_INVERSEMEANSFRAME_SIZE = 48*48;
    // const unsigned short kTRIGGER_EC_ASC_PMT_ROWS_SIZE = 18;
    // const unsigned short kTRIGGER_EC_ASC_PMT_COLS_SIZE = 2;
    // const unsigned short kTRIGGER_CHOSENTHRESHOLDS_SIZE = 2;
    // const unsigned short kTRIGGER_TRIGGERTHRESHOLD_ROWS_SIZE = 100;  
    // const unsigned short kTRIGGER_TRIGGERTHRESHOLD_COLS_SIZE = 5;  
    // const unsigned short kTRIGGER_EC_ASC_ROWS_SIZE = 144;
    // const unsigned short kTRIGGER_EC_ASC_COLS_SIZE = 16;
    
    Int_t RowArray = 0;
    Int_t ColArray = 0;

    for(Int_t i=0; i<kTRIGGER_EC_ASC_PMT_ROWS_SIZE; i++) {
        for(Int_t ii=0; ii<kTRIGGER_EC_ASC_PMT_COLS_SIZE; ii++) {
            validMaxForPMT[i][ii] = 0;
            invalidMaxForPMT[i][ii] = 0;
        }
    }

    FILE* file = 0;
    Float_t number_FLOAT = 0.f;
    
    for (Int_t u = 0; u < kTRIGGER_TRIGGERTHRESHOLD_ROWS_SIZE; u++) {
        for (Int_t uu = 0; uu < kTRIGGER_TRIGGERTHRESHOLD_COLS_SIZE; uu++) {
            triggerThresholds[u][uu] = 0;
        }
    }
    
    if(fOptions.GetThresholdsFilePathname().Length() > 0) {
        file = fopen(fOptions.GetThresholdsFilePathname(), "r");
        if (!file) {
            std::cerr << "Cannot open thresholds file " << fOptions.GetThresholdsFilePathname() << std::endl;
            throw std::invalid_argument("Cannot open thresholds file.");
        }

        Int_t countElementTHR = 0;
        while (fscanf(file, "%f", &number_FLOAT) != EOF) {
            RowArray = countElementTHR / 5;
            ColArray = countElementTHR % 5;
            if (RowArray > kTRIGGER_TRIGGERTHRESHOLD_ROWS_SIZE || ColArray > kTRIGGER_TRIGGERTHRESHOLD_COLS_SIZE) {
                throw std::length_error(TString::Format("Invalid range of input data. Index [%d][%d] (%d), input file \"%s\".", RowArray, ColArray, countElementTHR,   fOptions.GetThresholdsFilePathname().Data()).Data());
                break;
            }
            triggerThresholds[RowArray][ColArray] = number_FLOAT;
            countElementTHR++;
        }
        fclose(file);
    }
    else {
        throw std::invalid_argument("Thresholds file path is empty.");
    }
    
    for (Int_t uu = 0; uu < kTRIGGER_EC_ASC_ROWS_SIZE; uu++) {
        for (Int_t uuu = 0; uuu < kTRIGGER_EC_ASC_COLS_SIZE; uuu++) {
            maskPlot[uu][uuu] = 0.;
        }
    }

    Int_t countElementMask = 0;
    
    if(fOptions.GetMaskFilePathname().Length() > 0) {
        file = fopen(fOptions.GetMaskFilePathname(), "r"); // trigger Threshold file
        if (!file) {
            std::cerr << "Cannot open mask file " << fOptions.GetMaskFilePathname() << std::endl;
            throw std::invalid_argument("Cannot open mask file.");
        }

        while (fscanf(file, "%f", &number_FLOAT) != EOF) {
            ColArray = countElementMask % 16; //Organized as in Bertaina code 16 columns!
            RowArray = (countElementMask / 16) % 144; // 144 rows each PDM
            if (RowArray > kTRIGGER_EC_ASC_ROWS_SIZE || ColArray > kTRIGGER_EC_ASC_COLS_SIZE) {
                throw std::length_error(TString::Format("Invalid range of input data. Index [%d][%d] (%d), input file \"%s\".", RowArray, ColArray, countElementMask,   fOptions.GetMaskFilePathname().Data()).Data());
                break;
            }
            maskPlot[RowArray][ColArray] = number_FLOAT;
            countElementMask++;
        }
        fclose(file);
    }
    
    for (Int_t uu = 0; uu < kTRIGGER_EC_ASC_PMT_ROWS_SIZE; uu++) {
        for (Int_t uuu = 0; uuu < kTRIGGER_EC_ASC_PMT_COLS_SIZE; uuu++) {
            minThresholdsForPMT[uu][uuu] = 0.;
            maxThresholdsForPMT[uu][uuu] = 0.;
        }
    }
    
    Int_t countElementMinThresholds = 0;
    
    if(fOptions.GetMinSignalsFilePathname().Length() > 0) {
        file = fopen(fOptions.GetMinSignalsFilePathname(), "r"); // minThreshold file
        if (!file) {
            std::cerr << "Cannot open minThresholds file " << fOptions.GetMinSignalsFilePathname() << std::endl;
            throw std::invalid_argument("Cannot open minThresholds file.");
        }

        while (fscanf(file, "%f", &number_FLOAT) != EOF) {
            RowArray = countElementMinThresholds % 18; //18 PMT per row
            ColArray = (countElementMinThresholds / 18) % 2; // 2 rows
            if (RowArray > kTRIGGER_EC_ASC_PMT_ROWS_SIZE || ColArray > kTRIGGER_EC_ASC_PMT_COLS_SIZE) {
                throw std::length_error(TString::Format("Invalid range of input data. Index [%d][%d] (%d), input file \"%s\".", RowArray, ColArray, countElementMinThresholds,   fOptions.GetMinSignalsFilePathname().Data()).Data());
                break;
            }
            minThresholdsForPMT[RowArray][ColArray] = number_FLOAT;
            countElementMinThresholds++;
        }
        fclose(file);
    }

    Int_t countElementMaxThresholds = 0;
    
    if(fOptions.GetMaxSignalsFilePathname().Length() > 0) {
        file = fopen(fOptions.GetMaxSignalsFilePathname(), "r"); // maxThreshold file
        if (!file) {
            std::cerr << "Cannot open maxThresholds file " << fOptions.GetMaxSignalsFilePathname() << std::endl;
            throw std::invalid_argument("Cannot open maxThresholds file.");
        }

        while (fscanf(file, "%f", &number_FLOAT) != EOF) {
            RowArray = countElementMaxThresholds % 18; //18 PMT per row
            ColArray = (countElementMaxThresholds / 18) % 2; // 2 rows
            if (RowArray > kTRIGGER_EC_ASC_PMT_ROWS_SIZE || ColArray > kTRIGGER_EC_ASC_PMT_COLS_SIZE) {
                throw std::length_error(TString::Format("Invalid range of input data. Index [%d][%d] (%d), input file \"%s\".", RowArray, ColArray, countElementMaxThresholds,   fOptions.GetMaxSignalsFilePathname().Data()).Data());
                break;
            }
            maxThresholdsForPMT[RowArray][ColArray] = number_FLOAT;
            countElementMaxThresholds++;
        }
        fclose(file);
    }
    
    for (Int_t uu = 0; uu < kTRIGGER_INVERSEMEANSFRAME_SIZE; uu++) {
        inverseMeansFrame[uu] = 1.;
    }

    Int_t countInverseMeansFrame = 0;
    
    if(fOptions.GetInverseMeansFramePathname().Length() > 0) {
        file = fopen(fOptions.GetInverseMeansFramePathname(), "r"); // maxThreshold file
        if (!file) {
            std::cerr << "Cannot open inverse means frame file " << fOptions.GetInverseMeansFramePathname() << std::endl;
            throw std::invalid_argument("Cannot open inverse means frame file.");
        }

        while (fscanf(file, "%f", &number_FLOAT) != EOF) {
            if (countInverseMeansFrame > kTRIGGER_INVERSEMEANSFRAME_SIZE) { 
                throw std::length_error(TString::Format("Invalid range of input data. Index [%d], input file \"%s\".", countInverseMeansFrame,   fOptions.GetInverseMeansFramePathname().Data()).Data());
                break;
            }
            inverseMeansFrame[countInverseMeansFrame] = number_FLOAT;
            countInverseMeansFrame++;
        }
        fclose(file);
    } 
    else if (fOptions.IsInverseMeansFrame()) {
        throw std::invalid_argument("Missing inverse means file path.");
    }
    
    for(Int_t j = 0; j < 18; j++) {
        for(Int_t jj = 0; jj < 2 ; jj++) {
            thresholdsForPMT[j][jj] = -1.0;
            thresholdsForPMTGlobalGTU[j][jj] = 0.0;
            validTresholdsForPMT[j][jj] = false;
            chosenThresholds[0][j][jj] = 63; //initialize at maximum
            chosenThresholds[1][j][jj] = 512; //initialize at maximum
        }
    }

}

Trigger::~Trigger() { }

Double_t Trigger::GetXUpValueForHistogram(Int_t maxValue, Int_t numberOfBean) {
    return std::ceil( ((double)maxValue / (numberOfBean-1)) * 100) ;
}


std::string Trigger::GetTime() {
    time_t t;
    struct tm * ptr;
    Char_t buf[50];
    time(&t);
    ptr = localtime(&t);
    strftime(buf, 20, "%Y-%m-%d_%H-%M-%S", ptr);
    std::string bufString = buf;
    return bufString;
}

void Trigger::CreateHistogramAndGraphForMaxForPMT() {
    std::map<Int_t, Int_t> distributionMaxForPMT;

    //Graph
    for(Int_t i=0; i<18; i++) {
        for(Int_t ii=0; ii<2; ii++) {
            TCanvas *c1 = new TCanvas("c1" ,"", 1500, 1000);
            Int_t count = maxForPMTvalues[i][ii].size();
            Float_t x[count];
            Float_t y[count];

            Int_t j = 0;
            for(std::vector<Float_t>::iterator it = maxForPMTvalues[i][ii].begin(); it != maxForPMTvalues[i][ii].end(); it++) {
                x[j] = j;
                y[j] = *it;
                std::map<Int_t, Int_t>::iterator iterator;
                iterator = distributionMaxForPMT.find(*it);
                if(iterator == distributionMaxForPMT.end()) {
                    distributionMaxForPMT.insert(std::pair<Float_t,Int_t>((Int_t)*it, 1));
                }
                else {
                    iterator->second += 1;
                }
                j++;
            }

            TGraph *gr = new TGraph(count,x,y);
            std::string grafName = "MaxForPMT[" + std::to_string(i) + "][" + std::to_string(ii) +"]";
            gr->SetName(grafName.c_str());
            gr->GetXaxis()->SetTitle("packet");
            gr->GetYaxis()->SetTitle("count triggers");
            gr->Draw("A*");
            std::string fileName = "/MaxForPMTGraph[" + std::to_string(i) + "][" + std::to_string(ii) +"]-" + GetTime() + ".gif";
            std::string filePath =  fOptions.GetDataDir().Data() + fileName;
            c1->SaveAs(filePath.c_str());

            delete gr;
            delete c1;
        }
    }

    //Histogram
    TCanvas *c1 = new TCanvas("c1" ,"", 1500, 1000);
    c1->SetLogy(1);
    TH1I *histogramMaxForPMTDistribution = new TH1I("MaxForPMT Distribution", "MaxForPMT Distribution", 25, 0, 25+1);
    histogramMaxForPMTDistribution->GetXaxis()->SetTitle("Value");
    histogramMaxForPMTDistribution->GetYaxis()->SetTitle("Count");

    std::map<Int_t, Int_t>::iterator it = distributionMaxForPMT.begin();
    // Iterate over the map using Iterator till end.
    while (it != distributionMaxForPMT.end()) {
        histogramMaxForPMTDistribution->Fill(it->first, it->second);
        it++;
    }

    histogramMaxForPMTDistribution->Draw();
    std::string fileName = "/MaxForPMTHistogram-" + GetTime() + ".gif";
    std::string filePath =  fOptions.GetDataDir().Data() + fileName;
    c1->SaveAs(filePath.c_str());

    delete histogramMaxForPMTDistribution;
    delete c1;
}


void Trigger::CreateHistogramForThresholds() {
    TCanvas *c1 = new TCanvas("c1" ,"", 1500, 2000);
    c1->Divide(1,4);
    const Int_t NRGBs = 5;
    const Int_t NCont = 255;

    Double_t stops[NRGBs] = { 0.00, 0.34, 0.61, 0.84, 1.00 };
    Double_t red[NRGBs]   = { 0.00, 0.00, 0.87, 1.00, 0.51 };
    Double_t green[NRGBs] = { 0.00, 0.81, 1.00, 0.20, 0.00 };
    Double_t blue[NRGBs]  = { 0.51, 1.00, 0.12, 0.00, 0.00 };
    TColor::CreateGradientColorTable(NRGBs, stops, red, green, blue, NCont);
    gStyle->SetNumberContours(NCont);
    gStyle->SetPaintTextFormat("4.2f");

    TH2F *histogramMaxThresholdsForPMT = new TH2F("MaxThresholdsForPMT value", "MaxThresholdsForPMT value", 18+1, 0, 18+1, 2+1, 0, 2+1);
    histogramMaxThresholdsForPMT->GetXaxis()->SetTitle("MaxThresholdsForPMT - Rows");
    histogramMaxThresholdsForPMT->GetYaxis()->SetTitle("MaxThresholdsForPMT - Columns");
    histogramMaxThresholdsForPMT->SetStats(false);
    histogramMaxThresholdsForPMT->SetMarkerSize(3);

    TH2F *histogramMinThresholdsForPMT = new TH2F("MinThresholdsForPMT value", "MinThresholdsForPMT value", 18+1, 0, 18+1, 2+1, 0, 2+1);
    histogramMinThresholdsForPMT->GetXaxis()->SetTitle("MinThresholdsForPMT - Rows");
    histogramMinThresholdsForPMT->GetYaxis()->SetTitle("MinThresholdsForPMT - Columns");
    histogramMinThresholdsForPMT->SetStats(false);
    histogramMinThresholdsForPMT->SetMarkerSize(3);

    TH2I *histogramMaxForPMTValid = new TH2I("MaxForPMT Valid value", "MaxForPMT Valid value", 18+1, 0, 18+1, 2+1, 0, 2+1);
    histogramMaxForPMTValid->GetXaxis()->SetTitle("MaxForPMT - Rows");
    histogramMaxForPMTValid->GetYaxis()->SetTitle("MaxForPMT - Columns");
    histogramMaxForPMTValid->SetStats(false);

    TH2I *histogramMaxForPMTInvalid = new TH2I("MaxForPMT Invalid value", "MaxForPMT Invalid value", 18+1, 0, 18+1, 2+1, 0, 2+1);
    histogramMaxForPMTInvalid->GetXaxis()->SetTitle("MaxForPMT - Rows");
    histogramMaxForPMTInvalid->GetYaxis()->SetTitle("MaxForPMT - Columns");
    histogramMaxForPMTInvalid->SetStats(false);

    for(Int_t j = 0; j < 18; j++) {
        for(Int_t jj = 0; jj < 2 ; jj++) {
            histogramMaxThresholdsForPMT->Fill(j, jj, (Float_t)maxThresholdsForPMT[j][jj]);
            histogramMinThresholdsForPMT->Fill(j, jj, (Float_t)minThresholdsForPMT[j][jj]);
            histogramMaxForPMTValid->Fill(j, jj, validMaxForPMT[j][jj]);
            histogramMaxForPMTInvalid->Fill(j, jj, invalidMaxForPMT[j][jj]);
        }
    }

    c1->cd(1);
    histogramMaxThresholdsForPMT->Draw("TEXT");
    c1->cd(2);
    histogramMinThresholdsForPMT->Draw("TEXT");
    c1->cd(3);
    histogramMaxForPMTValid->Draw("colz");
    c1->cd(4);
    histogramMaxForPMTInvalid->Draw("colz");
    std::string fileName = "/MaxAndMinThresholdsForPMT-" + GetTime() + ".gif";
    std::string filePath = fOptions.GetDataDir().Data() + fileName;
    c1->SaveAs(filePath.c_str());

    delete histogramMaxForPMTInvalid;
    delete histogramMaxForPMTValid;
    delete histogramMaxThresholdsForPMT;
    delete histogramMinThresholdsForPMT;
    delete c1;
}

void Trigger::CreateHistogramForNumberOfFrame() {
    TCanvas *c1 = new TCanvas("c1","",1500,500);

    Int_t maxValue = 0;
    for(std::vector<Int_t>::iterator it = numberOfFrameForHistogram.begin(); it != numberOfFrameForHistogram.end(); it++) {
        if(maxValue < *it) {
            maxValue = *it;
        }
    }

    Int_t nBinsX = 100;
    Double_t xUp = GetXUpValueForHistogram(maxValue + 1, nBinsX);

    TH1I *histogramNumberOfFrame = new TH1I("NumberOfFrame - ThresholdsForPMTGlobalGTU", "NumberOfFrame - ThresholdsForPMTGlobalGTU", nBinsX, 0, xUp);
    histogramNumberOfFrame->GetXaxis()->SetTitle("GTU");
    histogramNumberOfFrame->GetYaxis()->SetTitle("Count");

    for(std::vector<Int_t>::iterator it = numberOfFrameForHistogram.begin(); it != numberOfFrameForHistogram.end(); it++) {
        histogramNumberOfFrame->Fill(*it);
    }
    numberOfFrameForHistogram.clear();

    histogramNumberOfFrame->Draw("");
    std::string fileName = "/NumberOfFrame-" + GetTime() + ".gif";
    std::string filePath =  fOptions.GetDataDir().Data() + fileName;
    c1->SaveAs(filePath.c_str());

    delete histogramNumberOfFrame;
    delete c1;
}

void Trigger::CreateHistogramForTriggerRate() {
    TCanvas *c1 = new TCanvas("c1","",1500,500);

    Int_t maxValue = 0;
    for(std::vector<Int_t>::iterator it = triggerRateForHistogram.begin(); it != triggerRateForHistogram.end(); it++) {
        if(maxValue < *it) {
            maxValue = *it;
        }
    }

    Int_t nBinsX = 500;
    Double_t xUp = GetXUpValueForHistogram(maxValue + 1, nBinsX);

    TH1I *histogramTriggerRate = new TH1I("TriggerRate", "TriggerRate", nBinsX, 0, xUp);
    histogramTriggerRate->GetXaxis()->SetTitle("GTU");
    histogramTriggerRate->GetYaxis()->SetTitle("Count");
    c1->SetLogy(1);

    for(std::vector<Int_t>::iterator it = triggerRateForHistogram.begin(); it != triggerRateForHistogram.end(); it++) {
        histogramTriggerRate->Fill(*it);
    }
    triggerRateForHistogram.clear();

    histogramTriggerRate->Draw("");
    std::string fileName = "/TriggerRate-" + GetTime() + ".gif";
    std::string filePath = fOptions.GetDataDir().Data() + fileName;
    c1->SaveAs(filePath.c_str());
    delete c1;
}

void Trigger::SetChosenTresholds(const Int_t row, const Int_t col, const Float_t &triggerBin) {
   Int_t triggerBinINT = (Int_t)triggerBin;
   if (triggerBinINT < 63) {
       if (triggerBinINT > 0) {
           chosenThresholds[0][row][col] = (Int_t)triggerThresholds[triggerBinINT][1];
           chosenThresholds[1][row][col] = (Int_t)triggerThresholds[triggerBinINT][4];
       } else {
           chosenThresholds[0][row][col] = (Int_t)triggerThresholds[0][1];
           chosenThresholds[1][row][col] = (Int_t)triggerThresholds[0][4];
       }
   } else {
       chosenThresholds[0][row][col] = 63;
       chosenThresholds[1][row][col] = 512;
   }
}

Int_t Trigger::GetCountValidPMT() {
    Int_t count = 0;
    for(Int_t i = 0; i < 18; i++) {
        for(Int_t ii = 0; ii < 2; ii++) {
            if(validTresholdsForPMT[i][ii] == true) {
                count++;
            }
        }
    }

    return count;
}

void Trigger::ReadThresholdsForFirstPacket(Float_t ThresholdLoweringFactor,
                                        Int_t packet_size,
                                        PhotonCountDataIterator<Int_t> *iterate) {
    // initialize variables for data
    Long_t countElement = 0;
    Int_t NumberOfFrame = 0;
    Int_t RowArray = 0;
    Int_t ColArray = 0;
    Int_t number;

    Float_t averageBackground[144][16];
    Float_t averageBackgroundUnit[144][16];
    for (Int_t uu = 0; uu < 144; uu++) {
       for (Int_t uuu = 0; uuu < 16; uuu++) {
           averageBackground[uu][uuu] = 0; //initialize
           averageBackgroundUnit[uu][uuu] = 0;
       }
    }

    Float_t maxForPMT[18][2];
    for (Int_t uu = 0; uu < 18; uu++) {
        for (Int_t uuu = 0; uuu < 2; uuu++) {
            maxForPMT[uu][uuu] = 0;
        }
    }

    for(PhotonCountDataIterator<Int_t>::const_iterator it = iterate->cbegin(); it != iterate->cend(); it++) {
        number = *it;
        Int_t packet_position = NumberOfFrame / packet_size;
        if(packet_position == 0) { //skip first packet
            if ((countElement + 1) % 2304 == 0) { //last pixel of PDM
                NumberOfFrame++;
            }
            countElement++;
            continue;
        } else if(packet_position == 2) { //end reading at third packet
            break;
        }

        ColArray = countElement % 16; //Organized as in Bertaina code 16 columns!
        RowArray = (countElement / 16) % 144; // 144 rows each PDM

        // applies the mask
        if (maskPlot[RowArray][ColArray] == 0) {
            number = 0;
        }

        // calculates average background map
        
        if(fOptions.IsInverseMeansFrame()) {
            Int_t index = countElement % 2304;
            averageBackground[RowArray][ColArray] += (number * inverseMeansFrame[index]); // increments background
        } else {
            averageBackground[RowArray][ColArray] += number; // increments background
        }
        
        averageBackgroundUnit[RowArray][ColArray]++;

        if ((NumberOfFrame + 1) % packet_size == 0) { //last GTU of block
            if (averageBackgroundUnit[RowArray][ColArray] > 0) {
                averageBackground[RowArray][ColArray] /= averageBackgroundUnit[RowArray][ColArray]; //
            } else {
                averageBackground[RowArray][ColArray] = 0.;
            }

            // calculate thresholds for each PMT
            if (averageBackground[RowArray][ColArray] > maxForPMT[RowArray / 8][ColArray / 8]) {
                maxForPMT[RowArray / 8][ColArray / 8] = averageBackground[RowArray][ColArray];
            }

            Float_t triggerBin = maxForPMT[RowArray / 8][ColArray / 8] * 10;
            triggerBin *= ThresholdLoweringFactor; // LOWERING THRESHOLDS

            SetChosenTresholds(RowArray / 8, ColArray / 8, triggerBin);
        }

        if((countElement + 1) % 2304 == 0) {  //last pixel of PDM
            if((NumberOfFrame + 1) % packet_size == 0){
                if(fOptions.IsUseMinAndMaxSignals()) { //last GTU of block
                    for(Int_t j = 0; j < 18; j++){
                        for(Int_t jj = 0; jj < 2; jj++) {
                            if(maxForPMT[j][jj] >= minThresholdsForPMT[j][jj] && maxForPMT[j][jj] <= maxThresholdsForPMT[j][jj]) {
                                // valid value
                                validTresholdsForPMT[j][jj] = true;
                                thresholdsForPMT[j][jj] = maxForPMT[j][jj];
                                thresholdsForPMTGlobalGTU[j][jj] = 0;
                            } else {
                                // invalid value
                                validTresholdsForPMT[j][jj] = false;
                                Float_t triggerBin = maxForPMT[j][jj] * 10;
                                triggerBin *= ThresholdLoweringFactor; // LOWERING THRESHOLDS
                                SetChosenTresholds(j, jj, triggerBin);
                            }
                        }
                    }
                }

                if(fOptions.IsUseThresholdForPdm()) {
                    if(fOptions.IsUseMinAndMaxSignals()) {
                        Float_t avgValue = 0.0f;
                        Int_t validValue = 0;
                        for(Int_t j = 0; j < 18; j++){
                            for(Int_t jj = 0; jj < 2; jj++) {
                                if(maxForPMT[j][jj] >= minThresholdsForPMT[j][jj] && maxForPMT[j][jj] <= maxThresholdsForPMT[j][jj]) {
                                    validValue++;
                                    avgValue += maxForPMT[j][jj];
                                }
                            }
                        }
                        if(validValue >= fOptions.GetMinNumValidPmt()) {
                            avgValue /= validValue;

                            Float_t triggerBin = avgValue * 10;
                            triggerBin *= ThresholdLoweringFactor; // LOWERING THRESHOLDS
                            for(Int_t j = 0; j < 18; j++) {
                                for(Int_t jj = 0; jj < 2; jj++) {
                                    SetChosenTresholds(j, jj, triggerBin);
                                }
                            }
                        } else {
                            throw std::logic_error("Not enough valid PMT in packet.");
                        }
                    } else {
                        Float_t avgValue;
                        Int_t countValue = 0;

                        for(Int_t j = 0; j < 18; j++){
                            for(Int_t jj = 0; jj < 2; jj++) {
                                countValue++;
                                avgValue += maxForPMT[j][jj];
                            }
                        }

                        avgValue /= countValue;
                        Float_t triggerBin = avgValue * 10.0f;
                        triggerBin *= ThresholdLoweringFactor; // LOWERING THRESHOLDS
                        for(Int_t j = 0; j < 18; j++) {
                            for(Int_t jj = 0; jj < 2; jj++) {
                                SetChosenTresholds(j, jj, triggerBin);
                            }
                        }
                    }
                }
            }
            
            if (NumberOfFrame == std::numeric_limits<Int_t>::max()) {
                throw std::runtime_error("Number of frame (NumberOfFrame) overflowed data type size (Int_t)");
            }
            
            NumberOfFrame++;
            fGtuCounter++;
        }
            
        if (countElement == std::numeric_limits<Long_t>::max()) {
            throw std::runtime_error("Number of element (countElement) overflowed data type size (Long_t)");
        }
        
        countElement++;
    }
}

void Trigger::FenuRootOut(Float_t ThresholdLoweringFactor, Int_t packet_size, 
                           PhotonCountDataIterator<Int_t> *iterate,
                           const char* outputrootfile, const char* fname_stdout_prefix) {
    
    // These are defined in the header file:
    // const unsigned short kTRIGGER_INVERSEMEANSFRAME_SIZE = 48*48;
    // const unsigned short kTRIGGER_EC_ASC_PMT_ROWS_SIZE = 18;
    // const unsigned short kTRIGGER_EC_ASC_PMT_COLS_SIZE = 2;
    // const unsigned short kTRIGGER_CHOSENTHRESHOLDS_SIZE = 2;
    // const unsigned short kTRIGGER_TRIGGERTHRESHOLD_ROWS_SIZE = 100;  
    // const unsigned short kTRIGGER_TRIGGERTHRESHOLD_COLS_SIZE = 5;  
    // const unsigned short kTRIGGER_EC_ASC_ROWS_SIZE = 144;
    // const unsigned short kTRIGGER_EC_ASC_COLS_SIZE = 16;
        
    // Provide a data file oganized as follows:
    // Left to right and top to  bottom
    // EC by EC also LR TB
    // 1 PDM for each GTU in one block
    // 16 columns X 16 rows / EC
    // after 144 rows new frame
    // conversion LECH to MARIO needed!!!!!
    Int_t arraySaveData[5][144][16];
    for (Int_t u = 0; u < 5; u++) {
        for (Int_t uu = 0; uu < 144; uu++) {
            for (Int_t uuu = 0; uuu < 16; uuu++) {
                arraySaveData[u][uu][uuu] = 0; //initialize
            }
        }
    }

    // initialize variables for data
    Long_t countElement = 0;
    Int_t NumberOfFrame = 0;
    Int_t ArrayLoopFrameNumber = 0;
    Int_t RowArray = 0;
    Int_t ColArray = 0;
    Int_t number;
    Int_t triggerRate = 0;

    //initialize average background measurement persistency and 3X3 sum
    Int_t persistency[144][16];
    Int_t Sum3X3[144][16];
    Int_t persistencyFlag[144][16];
    Float_t averageBackground[144][16];
    Float_t averageBackgroundUnit[144][16];
    for (Int_t uu = 0; uu < 144; uu++) {
        for (Int_t uuu = 0; uuu < 16; uuu++) {
            averageBackground[uu][uuu] = 0; //initialize
            averageBackgroundUnit[uu][uuu] = 0;
            persistency[uu][uuu] = 0;
            persistencyFlag[uu][uuu] = 0;
            Sum3X3[uu][uuu] = 0;
        }
    }

    Float_t maxForPMT[18][2];

    for (Int_t uu = 0; uu < 18; uu++) {
        for (Int_t uuu = 0; uuu < 2; uuu++) {
            maxForPMT[uu][uuu] = 0;
        }
    }

    std::cout << fname_stdout_prefix << "Triggering start!" << std::endl;

    // OUTPUT ROOT FILE

//     TString outputrootfile;
//     outputrootfile = outputprefix;
//     outputrootfile += ".root";

    TFile* froot = new TFile(outputrootfile, "recreate");

    TTree* thrtable = new TTree("thrtable", "Threshld table");
    TTree* gtusry = new TTree("gtusry", "GTU summary");
    TTree* l1trg = new TTree("l1trg", "LT1 Trigger info");

    Int_t ecID, pmtRow, pmtCol, pixRow, pixCol, gtuGlobal, packetID, gtuInPacket, sumL1, thrL1, persistL1;
    Int_t sumL1PDM;

    Int_t ntrigPerGTU[MAXGTUINFILE];
    Int_t trgBoxPerGTU;
    Int_t trgPMTPerGTU;
    Int_t trgECPerGTU;

    static UInt_t trg_yesPMT[18][2][MAXGTUINFILE];
    static UInt_t trg_yesEC[9][MAXGTUINFILE];

    static UInt_t sumL1PMT[18][2];
    static UInt_t sumL1EC[9];
    Int_t nPersist, gtuInPersist;

    thrtable->Branch("triggerThresholds", triggerThresholds, "triggerThresholds[100][5]/F");
    thrtable->Branch("ave_bg_factor", &ThresholdLoweringFactor, "ave_bg_factor/F");

    l1trg->Branch("ecID", &ecID, "ecID/I");
    l1trg->Branch("pmtRow", &pmtRow, "pmtRow/I");
    l1trg->Branch("pmtCol", &pmtCol, "pmtCol/I");
    l1trg->Branch("pixRow", &pixRow, "pixRow/I");
    l1trg->Branch("pixCol", &pixCol, "pixCol/I");
    l1trg->Branch("gtuGlobal", &gtuGlobal, "gtuGlobal/I");
    l1trg->Branch("packetID", &packetID, "packetID/I");
    l1trg->Branch("gtuInPacket", &gtuInPacket, "gtuInPacket/I");
    l1trg->Branch("sumL1", &sumL1, "sumL1/I");
    l1trg->Branch("thrL1", &thrL1, "thrL1/I");
    l1trg->Branch("persistL1", &persistL1, "persistL1/I");

    gtusry->Branch("gtuGlobal", &gtuGlobal, "gtuGlobal/I");
    gtusry->Branch("trgBoxPerGTU", &trgBoxPerGTU, "trgBoxPerGTU/I");
    gtusry->Branch("trgPMTPerGTU", &trgPMTPerGTU, "trgPMTPerGTU/I");
    gtusry->Branch("trgECPerGTU", &trgECPerGTU, "trgECPerGTU/I");
    gtusry->Branch("nPersist", &nPersist, "nPersist/I");
    gtusry->Branch("gtuInPersist", &gtuInPersist, "gtuInPersist/I");

    gtusry->Branch("sumL1PDM", &sumL1PDM, "sumL1PDM/I");
    gtusry->Branch("sumL1EC", sumL1EC, "sumL1EC[9]/I");
    gtusry->Branch("sumL1PMT", sumL1PMT, "sumL1PMT[18][2]/I");

    Int_t trgPMT[18][2];
    gtusry->Branch("trgPMT", trgPMT, "trgPMT[18][2]/I");

    thrtable->Fill(); // saved with correction factor

    for (Int_t kkk = 0; kkk < MAXGTUINFILE; kkk++) {
        for (Int_t iii = 0; iii < 18; iii++) {
            trg_yesPMT[iii][0][kkk] = trg_yesPMT[iii][1][kkk] = 0;
        }
        for (Int_t jjj = 0; jjj < 9; jjj++) {
            trg_yesEC[jjj][kkk] = 0;
        }
    }

    for(PhotonCountDataIterator<Int_t>::const_iterator it = iterate->cbegin(); it != iterate->cend(); it++) {
        number = *it;
        // allgtus++;
        ColArray = countElement % 16; //Organized as in Bertaina code 16 columns!
        RowArray = (countElement / 16) % 144; // 144 rows each PDM
        ArrayLoopFrameNumber = NumberOfFrame % 5; //if buffer is 5GTU deep! 5 is not uses! Just 2 in this configuration!
        
        // applies the mask
        if (maskPlot[RowArray][ColArray] == 0) {
            number = 0;
        }

        // once GTU block (128 GTU) is processed reset the maps
        if ((countElement + 1) % (2304 * packet_size) == 0) { //just at last pixel-GTU of block
            for (Int_t col = 0; col < 16; col++) {
                for (Int_t row = 0; row < 144; row++) {
                    persistency[row][col] = 0.;
                    persistencyFlag[row][col] = 0;
                    Sum3X3[row][col] = 0;
                    for (Int_t t = 0; t < 5; t++)
                        arraySaveData[t][row][col] = 0;
                }
            }
        }

        // calculates average background map
        if(fOptions.IsInverseMeansFrame()) {
            Int_t index = countElement % 2304;
            averageBackground[RowArray][ColArray] += (number * inverseMeansFrame[index]); // increments background
        } else {
            averageBackground[RowArray][ColArray] += number; // increments background
        }
        averageBackgroundUnit[RowArray][ColArray]++;

        if ((NumberOfFrame + 1) % packet_size == 0) {//last GTU of block
            if (averageBackgroundUnit[RowArray][ColArray] > 0) {
                averageBackground[RowArray][ColArray] /= averageBackgroundUnit[RowArray][ColArray];
            }
            else {
                averageBackground[RowArray][ColArray] = 0.;
            }

            // calculate thresholds for  each PMT
            if (averageBackground[RowArray][ColArray] > maxForPMT[RowArray / 8][ColArray / 8]) {
                maxForPMT[RowArray / 8][ColArray / 8] = averageBackground[RowArray][ColArray];
            }

            Float_t triggerBin = maxForPMT[RowArray / 8][ColArray / 8] * 10;
            triggerBin *= ThresholdLoweringFactor; // LOWERING THRESHOLDS

            SetChosenTresholds(RowArray / 8, ColArray / 8, triggerBin);
        }

        //saves data in loop array
        arraySaveData[ArrayLoopFrameNumber][RowArray][ColArray] = number;

        //Now all the data for 1 PDM are saved! Start trigger!
        if ((countElement + 1) % 2304 == 0) { //last pixel of PDM
            
//             std::cout << "> countElement=" << countElement << " NumberOfFrame=" << NumberOfFrame << std::endl;
            
            for (Int_t col = 0; col < 16; col++) {
                for (Int_t row = 0; row < 144; row++) {
                    persistencyFlag[row][col] = 0;
                }
            }

            if(fOptions.IsTriggerVisual()) {
                if ((NumberOfFrame + 1) % packet_size == 0 ) {
                    for(Int_t i = 0; i < 18; i++) {
                        for(Int_t ii = 0; ii < 2; ii++) {
                            maxForPMTvalues[i][ii].push_back(maxForPMT[i][ii]);
                        }
                    }
                }
            }

            if ((NumberOfFrame + 1) % packet_size == 0 && fOptions.IsUseMinAndMaxSignals()) { //last GTU of block
                for(Int_t j = 0; j < 18; j++){
                    for(Int_t jj = 0; jj < 2; jj++) {
                        if(maxForPMT[j][jj] >= minThresholdsForPMT[j][jj] && maxForPMT[j][jj] <= maxThresholdsForPMT[j][jj]) {
                            // valid value
                            validTresholdsForPMT[j][jj] = true;
                            thresholdsForPMT[j][jj] = maxForPMT[j][jj];
                            thresholdsForPMTGlobalGTU[j][jj] = NumberOfFrame;

                            validMaxForPMT[j][jj] = validMaxForPMT[j][jj] + 1;
                        }
                        else {
                            // invalid value
                            Int_t diffBetweenGTU = fGtuCounter - NumberOfFrame;
                            if(thresholdsForPMT[j][jj] < 0.0f) {
                                validTresholdsForPMT[j][jj] = false;
                                invalidMaxForPMT[j][jj] = invalidMaxForPMT[j][jj] + 1;
                            } else if (diffBetweenGTU <= 10000000) { // zmeniť na inu hodnotu
                                maxForPMT[j][jj] = thresholdsForPMT[j][jj];

                                if(fOptions.IsTriggerVisual()) {
                                    numberOfFrameForHistogram.push_back(diffBetweenGTU);
                                }
                                invalidMaxForPMT[j][jj] = invalidMaxForPMT[j][jj] + 1;

                                Float_t triggerBin = maxForPMT[j][jj] * 10;
                                triggerBin *= ThresholdLoweringFactor; // LOWERING THRESHOLDS
                                SetChosenTresholds(j, jj, triggerBin);
                            }
                            else {
                                throw std::logic_error("Try to use old thresholds value.");
                            }
                        }
                    }
                }
            }

            if ((NumberOfFrame + 1) % packet_size == 0 && fOptions.IsUseThresholdForPdm()) { //last GTU of block
                if(fOptions.IsUseMinAndMaxSignals()) {
                    Float_t avgValue = 0.0f;
                    Int_t validValue = 0;
                    for(Int_t j = 0; j < 18; j++){
                        for(Int_t jj = 0; jj < 2; jj++) {
                            if(maxForPMT[j][jj] >= minThresholdsForPMT[j][jj] && maxForPMT[j][jj] <= maxThresholdsForPMT[j][jj]) {
                                validValue++;
                                avgValue += maxForPMT[j][jj];
                            }
                        }
                    }
                    if(validValue >= fOptions.GetMinNumValidPmt()) {
                        avgValue /= validValue;

                        Float_t triggerBin = avgValue * 10.0f;
                        triggerBin *= ThresholdLoweringFactor; // LOWERING THRESHOLDS
                        for(Int_t j = 0; j < 18; j++) {
                            for(Int_t jj = 0; jj < 2; jj++) {
                                SetChosenTresholds(j, jj, triggerBin);
                            }
                        }
                    } else {
                        throw std::logic_error("Not enough valid PMT in packet.");
                    }
                } else {
                    Float_t avgValue = 0.0f;
                    Int_t countValue = 0;
                    for(Int_t j = 0; j < 18; j++){
                        for(Int_t jj = 0; jj < 2; jj++) {
                            countValue++;
                            avgValue += maxForPMT[j][jj];
                        }
                    }
                    avgValue /= countValue;
                    Float_t triggerBin = avgValue * 10.0f;
                    triggerBin *= ThresholdLoweringFactor; // LOWERING THRESHOLDS
                    for(Int_t j = 0; j < 18; j++) {
                        for(Int_t jj = 0; jj < 2; jj++) {
                            SetChosenTresholds(j, jj, triggerBin);
                        }
                    }
                }
            }

            if(fOptions.IsUseMinAndMaxSignals() && GetCountValidPMT() < fOptions.GetMinNumValidPmt()) {
                throw std::logic_error("Not enough valid PMT in packet.");
            }

            // TRIGGER  algorithm
            
            // When a pixel in a cell detects a number of counts equal, or higher
            // than, a preset threshold N, the FLT logic checks if for a certain number of following GTUs (R) in
            // a slot of consecutive GTUs (P), there is at least one pixel in the same cell with an activity equal
            // to, or higher than N, and the total number of photo-electrons integrated in the cell is higher than
            // a preset value S
            // https://pos.sissa.it/301/443/pdf
            
            for (Int_t PMTNumCOL = 0; PMTNumCOL < 2; PMTNumCOL++) {
                for (Int_t PMTNumROW = 0; PMTNumROW < 18; PMTNumROW++) {
                    if(validTresholdsForPMT[PMTNumROW][PMTNumCOL] == false && fOptions.IsUseMinAndMaxSignals()) {
                        continue;
                    }
                    for (Int_t InternalPMTROW = 1; InternalPMTROW < 7; InternalPMTROW++) {
                        for (Int_t InternalPMTCOL = 1; InternalPMTCOL < 7; InternalPMTCOL++) {
                            Int_t pixROW = (PMTNumROW * 8) + InternalPMTROW;
                            Int_t pixCOL = (PMTNumCOL * 8) + InternalPMTCOL;

                            Sum3X3[pixROW][pixCOL] = 0; //nuovo // adder S
                            persistency[pixROW][pixCOL] = 0; //nuovo  // adder R
                            
                            // N_{pst} = 2 // slot of consecutive GTUs - P
                            for (Int_t GTUBuff = 0; GTUBuff < 2; GTUBuff++) {
                                
                                persistencyFlag[pixROW][pixCOL] = 0; //nuovo
                                Int_t arrayBufNew = ArrayLoopFrameNumber - GTUBuff;

                                if (arrayBufNew < 0) {
                                    arrayBufNew += 5; //works just if array is 5 GTU deep
                                }
                                // one MAPMT hosts 36 cells
                                for (Int_t boxIntCol = -1; boxIntCol < 2; boxIntCol++) {
                                    for (Int_t boxIntRow = -1; boxIntRow < 2; boxIntRow++) {
                                        // n^{pix}_thr // N -  When a pixel in a cell detects a number of counts equal, or higher than, a preset threshold N ??? >= ???
                                        if (arraySaveData[arrayBufNew][pixROW + boxIntRow][pixCOL + boxIntCol] > chosenThresholds[0][pixROW / 8][pixCOL / 8] && maskPlot[pixROW + boxIntRow][pixCOL + boxIntCol] > 0) {
                                            Sum3X3[pixROW][pixCOL] += arraySaveData[arrayBufNew][pixROW + boxIntRow][pixCOL + boxIntCol] - chosenThresholds[0][pixROW / 8][pixCOL / 8]; // adder S
                                            persistencyFlag[pixROW][pixCOL] = 1; //nuovo  
                                        }
                                    }
                                }

                                if (persistencyFlag[pixROW][pixCOL] == 1) {//nuovo
                                    persistency[pixROW][pixCOL]++; //nuovo // adder R
                                }
                            }

                            //nuovo/////////////////
                            
                            // N_{cdt} = 2  // number of following GTUs - R
                            if (persistency[pixROW][pixCOL] >= 2) {
                                // n^{cell}_{thr} // the total number of photo-electrons integrated in the cell is higher than a preset value S
                                if (Sum3X3[pixROW][pixCOL] > chosenThresholds[1][pixROW / 8][pixCOL / 8]) {
                                    Int_t diffBetweenGTU = std::abs(triggerRate - NumberOfFrame);
                                    triggerRate = NumberOfFrame;

                                    if(fOptions.IsTriggerVisual()) {
                                        triggerRateForHistogram.push_back(diffBetweenGTU);
                                    }
                                    // N^{thr}_{GTU} is not here
                                    ecID = PMTNumROW / 2 + 1;
                                    pmtRow = PMTNumROW;
                                    pmtCol = PMTNumCOL;
                                    pixRow = InternalPMTROW;
                                    pixCol = InternalPMTCOL;
                                    gtuGlobal = NumberOfFrame;
                                    packetID = NumberOfFrame / packet_size;
                                    gtuInPacket = NumberOfFrame % packet_size;
                                    sumL1 = Sum3X3[pixROW][pixCOL];
                                    thrL1 = chosenThresholds[1][pixROW / 8][pixCOL / 8];
                                    persistL1 = persistency[pixROW][pixCOL];
                                    l1trg->Fill();
                                    
                                    // std::cout << fname_stdout_prefix << "TRIGGER! ecID=" << ecID << " pmtRow=" << pmtRow << " pmtCol=" << pmtCol << " pixRow=" << pixRow << " pixCol=" << pixCol << " gtuGlobal=" << gtuGlobal << " packetID=" << packetID << " gtuInPacket=" << gtuInPacket << " sumL1=" << sumL1 << " thrL1=" << thrL1 << " persistL1=" << persistL1 << std::endl;
                                    
                                    if (gtuGlobal < MAXGTUINFILE) {
                                        trg_yesPMT[pmtRow][pmtCol][gtuGlobal] += sumL1;
                                        trg_yesEC[ecID - 1][gtuGlobal] += sumL1;

                                        ntrigPerGTU[gtuGlobal]++;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            
            if (NumberOfFrame == std::numeric_limits<Int_t>::max()) {
                throw std::runtime_error("Number of frame (NumberOfFrame) overflowed data type size (Int_t)");
            }
            
            NumberOfFrame++;
            fGtuCounter++;
        }

        if ((countElement + 1) % (2304 * packet_size) == 0) {
            for (Int_t col = 0; col < 16; col++) {
                for (Int_t row = 0; row < 144; row++) {
                    averageBackground[row][col] = 0.; //initialize
                    averageBackgroundUnit[row][col] = 0;
                    maxForPMT[row / 8][col / 8] = 0.;
                }
            }
        }

        if ((countElement + 1) % (packet_size * 2304 * 10) == 0) {
            printf("%sPacket %d completed \n", fname_stdout_prefix, NumberOfFrame / packet_size);
        }
            
        if (countElement == std::numeric_limits<Long_t>::max()) {
            throw std::runtime_error("Number of element (countElement) overflowed data type size (Long_t)");
        }

        countElement++;
    }

    if (NumberOfFrame > MAXGTUINFILE) {
        std::cout << fname_stdout_prefix << "Number of frames is greater than " << MAXGTUINFILE << " GTU summary (gtusry) statistics are calculated on truncated dataset (NumberOfFrame=" << NumberOfFrame << ")" << std::endl;
    }
    
    Int_t maxIi = std::min(NumberOfFrame, MAXGTUINFILE);
    
    for (Int_t ii = 0; ii < maxIi; ii++) {
        gtuGlobal = ii;  // important for gtusry tree
        trgBoxPerGTU = ntrigPerGTU[ii];
        trgECPerGTU = trgPMTPerGTU = 0;
        sumL1PDM = 0;

        for (Int_t jj = 0; jj < 9; jj++) {
            trgECPerGTU += (trg_yesEC[jj][ii] > 0);
            sumL1PDM += trg_yesEC[jj][ii];
            sumL1EC[jj] = trg_yesEC[jj][ii];
        }

        for (Int_t kk = 0; kk < 18; kk++) {
            trgPMTPerGTU += (trg_yesPMT[kk][0][ii] > 0);
            trgPMTPerGTU += (trg_yesPMT[kk][1][ii] > 0);

            trgPMT[kk][0] = (trg_yesPMT[kk][0][ii] > 0);
            trgPMT[kk][1] = (trg_yesPMT[kk][1][ii] > 0);

            sumL1PMT[kk][0] = trg_yesPMT[kk][0][ii];
            sumL1PMT[kk][1] = trg_yesPMT[kk][1][ii];
        }

        Int_t previous = 0;
        Int_t future = 0;
        for (Int_t nij = ii - 1; nij >= 0; nij--) {
            if (nij >= 0 && ntrigPerGTU[nij] > 0) {
                previous++;
            }
            else
                break;
        }

        for (Int_t nij = ii + 1; nij < maxIi; nij++) { // allgtus / 2304  should equal to NumberOfFrame
            if (ntrigPerGTU[nij] > 0) {
                future++;
            }
            else {
                break;
            }
        }
        nPersist = previous + future + 1;
        gtuInPersist = previous;

        gtusry->Fill();
    }


    std::cout << fname_stdout_prefix << "Triggering done" << std::endl;

    l1trg->Write();
    thrtable->Write();
    gtusry->Write();
    
    froot->Close();
    delete froot;
}
