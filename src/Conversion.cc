/*
 * Conversion.cc
 *
 *  Created on: Feb 18, 2018
 *      Author: Kristián Goliaš
 */

#include "Conversion.hh"

#include <stdexcept>


Int_t Conversion::ConvertFile3(const char* simuout, const char* pdm_asc_pathname, const char* ec_asc_pathname) {
    //Give one root file as input as formatted by the EUSO-TA collaboration
    // root file as in 2015
    //
    //Converts from Lech to mario's format
    //PDM
    //EC from up right to bottom left
    Int_t answer = 1;

    FILE *out1,*out2;

//     pathnames.pdm_asc = outsuffix + "_pdm_asc.gz";
//     pathnames.ec_asc = outsuffix + "_ec_asc.gz";
    
    TString outPDM = TString::Format("gzip -c > %s", pdm_asc_pathname);
    TString outEC  = TString::Format("gzip -c > %s", ec_asc_pathname);

    out1 = popen(outPDM,"w");
    out2 = popen(outEC,"w");

    TFile * fsimu = TFile::Open(simuout);

    if(!fsimu || fsimu->IsZombie()) {
        throw std::runtime_error(TString::Format("File %s does not exist", simuout).Data());
    }

    TTree *etree = (TTree*)fsimu->Get("tevent");

    if(!etree || etree->IsZombie()) {
      throw std::runtime_error(TString::Format("File %s (simuout) does not contain \"tevent\" tree.", simuout).Data());
    }

    UChar_t data[1][1][48][48];
    Int_t data_INT[48][48];
    etree->SetBranchAddress("photon_count_data",data);
    Int_t nentries_simu = (Int_t)etree->GetEntries();
    
    std::cout << "Starting conversion!" << std::endl;
    std::cout  << nentries_simu/128 << " packets to convert" << std::endl;

    for(Int_t i=0;i<nentries_simu;i++) {//runs over GTUs
        etree->GetEntry(i);
        // rotate 90 degrees counter clockwise
        for(Int_t row=0;row<48;row++) { //writes PDM-wise format (Lech)
            for(Int_t col=0;col<48;col++) {
                data_INT[row][col]=(Int_t) (data[0][0][col][47-row]);
            }
        }

        if(answer==1) {
            for(Int_t row=0;row<48;row++) {//writes PDM-wise format (Lech)
                for(Int_t col=0;col<48;col++) {
                    fprintf(out1,"%d ",data_INT[row][col]);
                }
                fprintf(out1,"\n");
            }
        }
        for(Int_t rowEC=0;rowEC<3;rowEC++) {//writes EC-wise format (Mario)
            for(Int_t colEC=0;colEC<3;colEC++) {
                for(Int_t row=0;row<16;row++) {
                    for(Int_t col=0;col<16;col++) {
                        fprintf(out2,"%d ",data_INT[rowEC*16+row][colEC*16+col]);
                    }
                    fprintf(out2,"\n");
                }
            }
        }
        if( i % (128 * 50) == 0 ) {
            std::cout << "Packet " << i/128 << " converted" << std::endl;
        }
    }

    std::cout  << nentries_simu/128 << " packets converted" << std::endl;
    std::cout << "Conversion Completed!" << std::endl;

    // fsimu->~TFile();
    delete fsimu;
    
    fclose(out1);
    fclose(out2);

    return 0;
}

Conversion::Conversion() { }

Conversion::~Conversion() { }
