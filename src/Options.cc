/*
 * Options.cc
 *
 *  Created on: Feb 4, 2018
 *      Author: Kristián Goliaš
 */

#include "Options.hh"

#include <iostream>
#include <stdexcept>
#include <cstdlib>

#include <getopt.h>


void Options::PrintHelp() {
    fprintf(stderr,"./a.out [option] allxxx*1.root allxx*2.root \n");
    fprintf(stderr,"--help, -h : show help \n");
    fprintf(stderr,"--dir, --outdir, --out, -o : output_directory (where output root file and selected images will store) \n");
    fprintf(stderr,"--overwrite, -w : run trigger event if output file exists and overwrite results \n");
    fprintf(stderr,"--bgfactor, -b : trigger_threshold_reduction_factor \n");
    fprintf(stderr,"--skip-convert, -C : no conversion to ascii from root \n");
    fprintf(stderr,"--skip-trigger, -T : skip trigger \n");
    fprintf(stderr,"--skip-visual, -V : no visualisation \n");
    fprintf(stderr,"--thresholds,-t : L1 triggere thresholds file table \n");
    fprintf(stderr,"--min-thresholds, -A : min thresholds value per PMT (18x2 format) \n");
    fprintf(stderr,"--max-thresholds, -B : max thresholds value per PMT (18x2 format) \n");
    fprintf(stderr,"--mask,-m : L1 triggere thresholds file table \n");
    fprintf(stderr,"--simple-prefix, -s : File name is not checked for specific name format, filename prefixed by \"trn\" and without extension is the prefix, squeeze is not checked \n");
    fprintf(stderr,"--ec-asc-input, -e : EC ASC format (mario) is expected as the input, conversion is skipped \n");
    fprintf(stderr,"--packet-size, -p : size of the packed, by default 128 \n");
    fprintf(stderr,"--max-file-name-length, -l : size of the packed, by default 128 \n");
    fprintf(stderr,"--handle-first-packet, -R : read thresholds for first packet (2nd packet from file)\n");
    fprintf(stderr,"--use-min-and-max-thresholds, -U : read thresholds for first packet (2nd packet from file)\n");
    fprintf(stderr,"--use-threshold-for-pdm, -D : tresholds PDM-wise\n");
    fprintf(stderr,"--min-num-valid-pmt, -E : minimal number of valid PMT in GTU\n");
    fprintf(stderr,"--inverse-means-frame, -G : pathname of calibration matrix applied to each frame\n");
}

Options::Options(Int_t argc, char **argv) {
    
    dataDir = "./";
    thresholdsFilePathname = "";
    minSignalsFilePathname = "";
    maxSignalsFilePathname = "";
    inverseMeansFramePathname = "";
    maskFilePathname = "./maskNOW.txt";
    help = false;
    overwrite = true;
    visualisation = true;
    convertYes = true;
    dotrigger = true;
    simplePrefix = false;
    ecAscInput = false;
    readThresholdsForFirstPacket = false;
    useMinAndMaxSignals = false;
    useThresholdForPDM = false;
    triggerVisual = false;
    inverseMeansFrame = false;
    packetSize = 128;
    ave_bg_factor = 1;
    minNumValidPMT = -1;
    maxFileNameLength = 255;

    int c;
    int option_index = 0;
    
    static struct option long_options[] =
            { { "help", no_argument, 0, 'h' },
              { "dir", required_argument, 0, 'o' },
              { "outdir", required_argument, 0, 'o' },
              { "out", required_argument, 0, 'o' },
              { "overwrite", required_argument, 0, 'w'},
              { "skip-convert", required_argument, 0, 'C' },
              { "skip-trigger", required_argument, 0, 'T' },
              { "skip-visual", required_argument, 0, 'V' },
              { "simple-prefix", required_argument, 0, 's' },
              { "ec-asc-input", required_argument, 0, 'e' },
              { "thresholds", required_argument, 0, 't' },
              { "min-signals", required_argument, 0, 'A' },
              { "max-signals", required_argument, 0, 'B' },
              { "mask", required_argument, 0, 'm' },
              { "bgfactor", required_argument, 0, 'b' },
              { "packet-size", required_argument, 0, 'p' },
              { "max-file-name-length", required_argument, 0, 'l' },
              { "handle-first-packet", required_argument, 0, 'R' },
              { "use-min-and-max-signals", required_argument, 0, 'U' }, // should be removed
              { "use-threshold-for-pdm", required_argument, 0, 'D' },
              { "min-num-valid-pmt", required_argument, 0, 'E'},
              { "trigger-visual", required_argument, 0, 'F'},
              { "inverse-means-frame", required_argument, 0, 'G'},
//               { "use-inverse-means-frame", required_argument, 0, 'H'},
              { 0, 0, 0, 0 }
            };

    do {
        c = getopt_long(argc, argv, "ho:w:C:T:V:s:e:t:A:B:m:b:p:l:R:U:D:E:F:G:H:", long_options, &option_index);
        
        if (c == -1) {
            break;
        }
            
        switch (c) {
            case 'h':
                help = true;
                break;
            case 'o':
                dataDir = TString(optarg);
                std::cout << "Output directory: " << dataDir << std::endl;
                break;
            case 'w':
                if(ArgIsFalse(optarg)) {
                    overwrite = false;
                } else if(ArgIsTrue(optarg)) {
                    overwrite = true;
                } else {
                    throw std::invalid_argument("Invalid argument value for overwrite");
                }
                std::cout << "Overwrite: " << overwrite << std::endl;
                break;
            case 'C':
                if(ArgIsFalse(optarg)) {
                    convertYes = true;
                } else if(ArgIsTrue(optarg)) {
                    convertYes = false;
                } else {
                    throw std::invalid_argument("Invalid argument value for skip-convert");
                }
                std::cout << "Skip conversion to ascii: " << !convertYes << std::endl;
                break;
            case 'T':
                if(ArgIsFalse(optarg)) {
                    dotrigger = true;
                } else if(ArgIsTrue(optarg)) {
                    dotrigger = false;
                } else {
                    throw std::invalid_argument("Invalid argument value for skip-trigger");
                }
                std::cout << "Skip trigger: " << !dotrigger << std::endl;
                break;
            case 'V':
                if(ArgIsFalse(optarg)) {
                    visualisation = true;
                } else if(ArgIsTrue(optarg)) {
                    visualisation = false;
                } else {
                    throw std::invalid_argument("Invalid argument value for skip-visual");
                }
                std::cout << "Skip visualisation: " << !visualisation << std::endl;
                break;
            case 's':
                if(ArgIsFalse(optarg)) {
                    simplePrefix = false;
                } else if(ArgIsTrue(optarg)) {
                    simplePrefix = true;
                } else {
                    throw std::invalid_argument("Invalid argument value for simple-prefix");
                }
                std::cout << "Use simple prefix: " << simplePrefix << std::endl;
                break;
            case 'e':
                if(ArgIsFalse(optarg)) {
                    ecAscInput = false;
                } else if(ArgIsTrue(optarg)) {
                    ecAscInput = true;
                } else {
                    throw std::invalid_argument("Invalid argument value for ec-asc-input");
                }
                std::cout << "Use EC ASC input: " << ecAscInput << std::endl;
                break;
            case 't':
                thresholdsFilePathname = TString(optarg);
                std::cout << "Thresholds file: " << thresholdsFilePathname << std::endl;
                break;
            case 'A':
                minSignalsFilePathname = TString(optarg);
                std::cout << "MinSignals file: " << minSignalsFilePathname << std::endl;
                break;
            case 'B':
                maxSignalsFilePathname = TString(optarg);
                std::cout << "MaxSignals file: " << maxSignalsFilePathname << std::endl;
                break;
            case 'm':
                maskFilePathname = TString(optarg);
                std::cout << "Mask file: " << maskFilePathname << std::endl;
                break;
            case 'b':
                ave_bg_factor = std::stof(optarg);
                std::cout << "BG threshold factor: " << ave_bg_factor << std::endl;
                break;
            case 'p':
                packetSize = atoi(optarg);
                std::cout << "Packet size: " << packetSize << std::endl;
                break;
            case 'l':
                maxFileNameLength = atoi(optarg);
                std::cout << "Max file name length: " << maxFileNameLength << std::endl;
                break;
            case 'R':
                if(ArgIsFalse(optarg)) {
                    readThresholdsForFirstPacket = false;
                } else if(ArgIsTrue(optarg)) {
                    readThresholdsForFirstPacket = true;
                } else {
                    throw std::invalid_argument("Invalid argument value for read thresholds for first packet");
                }
                std::cout << "Read thresholds for first packet: " << readThresholdsForFirstPacket << std::endl;
                break;
            case 'U':
                if(ArgIsFalse(optarg)) {
                    useMinAndMaxSignals = false;
                } else if(ArgIsTrue(optarg)) {
                    useMinAndMaxSignals = true;
                } else {
                    throw std::invalid_argument("Invalid argument value for use minimal and maximal thresholds");
                }
                std::cout << "Use minimal and maximal signals: " << useMinAndMaxSignals << std::endl;
                break;
            case 'D':
                if(ArgIsFalse(optarg)) {
                    useThresholdForPDM = false;
                } else if(ArgIsTrue(optarg)) {
                    useThresholdForPDM = true;
                } else {
                    throw std::invalid_argument("Invalid argument value for thresholds for PMT");
                }
                std::cout << "Thresholds for PMT: " << useThresholdForPDM << std::endl;
                break;
            case 'E':
                std::string::size_type sz;
                if(std::stoi(optarg,&sz) >= 0 && std::stoi(optarg,&sz) <= 32) {
                    minNumValidPMT = std::stoi (optarg,&sz);
                } else {
                    throw std::invalid_argument("Accepted-pmt could have value only between 0 and 32.");
                }
                std::cout << "Count accepted PMT: " << minNumValidPMT << std::endl;
                break;
            case 'F':
                if(ArgIsFalse(optarg)) {
                    triggerVisual = false;
                } else if(ArgIsTrue(optarg)) {
                    triggerVisual = true;
                } else {
                    throw std::invalid_argument("Invalid argument value for triggerVisual for PMT");
                }
                std::cout << "Trigger visual: " << triggerVisual << std::endl;
                break;
            case 'G':
                inverseMeansFramePathname = TString(optarg);
                std::cout << "Inverse means frame file: " << inverseMeansFramePathname << std::endl;
//                 break;
//             case 'H':
//                 if(ArgIsFalse(optarg)) {
//                     inverseMeansFrame = false;
//                 } else if(ArgIsTrue(optarg)) {
//                     inverseMeansFrame = true;
//                 } else {
//                     throw std::invalid_argument("Invalid argument value for coefficientMatrix");
//                 }
//                 std::cout << "Inverse means frame: " << inverseMeansFrame << std::endl;
                inverseMeansFrame =  (inverseMeansFramePathname.Length() > 0);
                break;
            default:
                help = true;
                if (optind > 1) {
                    throw std::invalid_argument(TString::Format("Invalid argument \"%s\" ('%c')", argv[optind-2], c).Data());
                }
                else {
                    throw std::logic_error("Invalid argument and unexpected optind");
                }
                break;
        }
    } while (c != -1);

    for (Int_t index = optind; index < argc; index++) {
        inputFiles.push_back(argv[index]);
        std::cout << "Input file: " << argv[index] << std::endl;
    }
}

Options::~Options() { }

Bool_t Options::ArgIsTrue(const char* optarg_v) const {
    return strcmp(optarg_v,"y")==0 || strcmp(optarg_v,"1")==0 || strcmp(optarg_v,"yes")==0 || strcmp(optarg_v,"true")==0 || strcmp(optarg_v,"True")==0;
}

Bool_t Options::ArgIsFalse(const char* optarg_v) const {
    return strcmp(optarg_v,"n")==0 || strcmp(optarg_v,"0")==0 || strcmp(optarg_v,"no")==0 || strcmp(optarg_v,"false")==0 || strcmp(optarg_v,"False")==0;
}


