/*
 * Visualization.cc
 *
 *  Created on: Feb 18, 2018
 *      Author: Kristián Goliaš
 */
#include "Visualization.hh"
#include "Palette.hh"

#define MAXGTUINFILE 500000

Visualization::Visualization() {
    pmtandgapmatrix = new TH2I("pmtandgapmatrix","",60,0,60,60,0,60);
    PDMFrame = new TH2D("PDMFrame","",6,-82.5,82.5,6,-82.5,82.5);
    framesnap = new TH2I("framesnap","",48,0,48,48,0,48);
    hist_count = new TH1I("hist_count","",1000,0,1000);
    trgframe = new TH2I("trgframe","",6,0,6,6,0,6);
    trgpmt = new TH2I("trgpmt","",6,0,6,6,0,6);
    trgec = new TH2I("trgec","",3,0,3,3,0,3);
    squareboxref = new TGraph(5);
    trgpmt_yes = new TH3I("trgpmt_yes","",6,0,6,6,0,6,128,-0.5,127.5);
    PDMTemplate = NULL;
    PDMGap = NULL;
}

Visualization::~Visualization() {
    delete pmtandgapmatrix;
    delete PDMFrame;
    delete framesnap;
    delete hist_count;
    delete trgframe;
    delete trgpmt;
    delete trgec;
    delete squareboxref;
    delete trgpmt_yes;
    if(PDMTemplate != NULL) {
        delete PDMTemplate;
    }
    if(PDMGap != NULL) {
        delete PDMGap;
    }
}

Int_t Visualization::pixcoordinateonmapbin(Int_t abspixx, Int_t abspixy){
    Int_t pmt_x,pmt_y;

    pmt_x = abspixx/8;
    pmt_y = abspixy/8;

    return pmtandgapmatrix->GetBin((pmt_y*10+abspixy%8+1)+1,pmt_x*10+abspixx%8+1);
}

Int_t Visualization::mesher(Int_t type){
    Bool_t TrigOnly=kTRUE;
    Int_t i,j;
    Int_t ii,jj;
    TGraph *mesherpmt[6][6];
    TGraph *mesherpix[48][48];

    Palette *palette = new Palette();
    palette->InvBPalette();

    for(i=0; i<6; i++) {
        for(j=0; j<6 ; j++) {
            mesherpmt[i][j] = new TGraph(5);
            mesherpmt[i][j]->SetPoint(0,-82.5+    i*27.5+2.23, 82.5-    j*27.5-2.23);
            mesherpmt[i][j]->SetPoint(1,-82.5+(i+1)*27.5-2.23, 82.5-    j*27.5-2.23);
            mesherpmt[i][j]->SetPoint(2,-82.5+(i+1)*27.5-2.23, 82.5-(j+1)*27.5+2.23);
            mesherpmt[i][j]->SetPoint(3,-82.5+    i*27.5+2.23, 82.5-(j+1)*27.5+2.23);
            mesherpmt[i][j]->SetPoint(4,-82.5+    i*27.5+2.23, 82.5-    j*27.5-2.23);
            mesherpmt[i][j]->Draw("same");

            for(ii=0; ii<8; ii++) {
                for( jj=0; jj<8; jj++) {
                    mesherpix[i*8+ii][j*8+jj] = new TGraph(5);
                    mesherpix[i*8+ii][j*8+jj]->SetPoint(0,-82.5+    i*27.5+2.23+    ii*2.88, 82.5-j*27.5-2.23-    jj*2.88);
                    mesherpix[i*8+ii][j*8+jj]->SetPoint(1,-82.5+    i*27.5+2.23+(ii+1)*2.88, 82.5-j*27.5-2.23-    jj*2.88);
                    mesherpix[i*8+ii][j*8+jj]->SetPoint(2,-82.5+    i*27.5+2.23+(ii+1)*2.88, 82.5-j*27.5-2.23-(jj+1)*2.88);
                    mesherpix[i*8+ii][j*8+jj]->SetPoint(3,-82.5+    i*27.5+2.23+    ii*2.88, 82.5-j*27.5-2.23-(jj+1)*2.88);
                    mesherpix[i*8+ii][j*8+jj]->SetPoint(4,-82.5+    i*27.5+2.23+    ii*2.88, 82.5-j*27.5-2.23-    jj*2.88);
                    if(type == 1) {
                        if(TrigOnly && ii >= 1 && ii <= 6 && jj >= 1 && jj <= 6) {
                            mesherpix[i*8+ii][j*8+jj]->SetLineColor(kGray);
                            mesherpix[i*8+ii][j*8+jj]->Draw("same");
                        }
                    }

                    if(type == 2) {
                        if(TrigOnly && ii >= 0 && ii <= 7 && jj >= 0 && jj <= 7) {
                            mesherpix[i*8+ii][j*8+jj]->SetLineColor(kGray);
                            mesherpix[i*8+ii][j*8+jj]->Draw("same");
                        }
                    }

                }
            }
        }
    }

    delete palette;
    return 0;
}

Int_t Visualization::drawtrgcentmap(Int_t type) {
    Int_t i,j;
    double PixelDimension[61];

    for(i=0; i<=60; i++) {
        PixelDimension[i] = (i/10)*27.5-82.5+(i%10!=0)*(2.23+2.88*((i-1)%10));
    }

    if(PDMTemplate == NULL) {
        PDMTemplate = new TH2D("PDMTemplate","",60,PixelDimension,60,PixelDimension);
    }

    PDMGap = (TH2D*)PDMTemplate->Clone("PDMGap");

    for(i=1; i<=60; i++) {
        for(j=1; j<=60; j++) {
            if((i-1)%10==0 || (j-1)%10==0 || (i-1)%10==9 || (j-1)%10==9) {
                PDMGap->SetBinContent(i,j,0);
            } else {
                PDMGap->SetBinContent(i,j,1);
            }
        }
    }

    PDMTemplate->SetMaximum(20);
    PDMTemplate->SetMinimum(0.5);
    Palette *palette = new Palette();
    palette->InvBPalette();
    PDMTemplate->Draw("colz");
    PDMFrame->GetXaxis()->SetTickLength(0);
    PDMFrame->GetYaxis()->SetTickLength(0);
    PDMFrame->GetXaxis()->SetLabelSize(0);
    PDMFrame->GetYaxis()->SetLabelSize(0);
    PDMFrame->Draw();
    mesher(type);

    delete palette;
    return 0;
}

Int_t Visualization::mario2pdm(Int_t mpmt_col, Int_t mpmt_row, Int_t pix_x, Int_t pix_y) {
    Int_t abs_pmtx;
    Int_t abs_pmty;

    if (mpmt_row >= 18 || mpmt_row < 0) {
        std::cerr << " Something is rotten in the state of row#. mario" << std::endl;
        return -999;
    }

    if (mpmt_col >= 2 || mpmt_col < 0) {
        std::cerr << " Something is rotten in the state of col#. mario" << std::endl;
        return -999;
    }

    //EC1
    if (mpmt_col == 0 && mpmt_row == 0) {
        abs_pmtx = 0;
        abs_pmty = 5;
    }
    if (mpmt_col == 1 && mpmt_row == 0) {
        abs_pmtx = 1;
        abs_pmty = 5;
    }
    if (mpmt_col == 0 && mpmt_row == 1) {
        abs_pmtx = 0;
        abs_pmty = 4;
    }
    if (mpmt_col == 1 && mpmt_row == 1) {
        abs_pmtx = 1;
        abs_pmty = 4;
    }

    //EC2
    if (mpmt_col == 0 && mpmt_row == 2) {
        abs_pmtx = 2;
        abs_pmty = 5;
    }
    if (mpmt_col == 1 && mpmt_row == 2) {
        abs_pmtx = 3;
        abs_pmty = 5;
    }
    if (mpmt_col == 0 && mpmt_row == 3) {
        abs_pmtx = 2;
        abs_pmty = 4;
    }
    if (mpmt_col == 1 && mpmt_row == 3) {
        abs_pmtx = 3;
        abs_pmty = 4;
    }

    //EC3
    if (mpmt_col == 0 && mpmt_row == 4) {
        abs_pmtx = 4;
        abs_pmty = 5;
    }
    if (mpmt_col == 1 && mpmt_row == 4) {
        abs_pmtx = 5;
        abs_pmty = 5;
    }
    if (mpmt_col == 0 && mpmt_row == 5) {
        abs_pmtx = 4;
        abs_pmty = 4;
    }
    if (mpmt_col == 1 && mpmt_row == 5) {
        abs_pmtx = 5;
        abs_pmty = 4;
    }

    //EC4
    if (mpmt_col == 0 && mpmt_row == 6) {
        abs_pmtx = 0;
        abs_pmty = 3;
    }
    if (mpmt_col == 1 && mpmt_row == 6) {
        abs_pmtx = 1;
        abs_pmty = 3;
    }
    if (mpmt_col == 0 && mpmt_row == 7) {
        abs_pmtx = 0;
        abs_pmty = 2;
    }
    if (mpmt_col == 1 && mpmt_row == 7) {
        abs_pmtx = 1;
        abs_pmty = 2;
    }

    //EC5
    if (mpmt_col == 0 && mpmt_row == 8) {
        abs_pmtx = 2;
        abs_pmty = 3;
    }
    if (mpmt_col == 1 && mpmt_row == 8) {
        abs_pmtx = 3;
        abs_pmty = 3;
    }
    if (mpmt_col == 0 && mpmt_row == 9) {
        abs_pmtx = 2;
        abs_pmty = 2;
    }
    if (mpmt_col == 1 && mpmt_row == 9) {
        abs_pmtx = 3;
        abs_pmty = 2;
    }

    //EC6
    if (mpmt_col == 0 && mpmt_row == 10) {
        abs_pmtx = 4;
        abs_pmty = 3;
    }
    if (mpmt_col == 1 && mpmt_row == 10) {
        abs_pmtx = 5;
        abs_pmty = 3;
    }
    if (mpmt_col == 0 && mpmt_row == 11) {
        abs_pmtx = 4;
        abs_pmty = 2;
    }
    if (mpmt_col == 1 && mpmt_row == 11) {
        abs_pmtx = 5;
        abs_pmty = 2;
    }

    //EC7
    if (mpmt_col == 0 && mpmt_row == 12) {
        abs_pmtx = 0;
        abs_pmty = 1;
    }
    if (mpmt_col == 1 && mpmt_row == 12) {
        abs_pmtx = 1;
        abs_pmty = 1;
    }
    if (mpmt_col == 0 && mpmt_row == 13) {
        abs_pmtx = 0;
        abs_pmty = 0;
    }
    if (mpmt_col == 1 && mpmt_row == 13) {
        abs_pmtx = 1;
        abs_pmty = 0;
    }

    //EC8
    if (mpmt_col == 0 && mpmt_row == 14) {
        abs_pmtx = 2;
        abs_pmty = 1;
    }
    if (mpmt_col == 1 && mpmt_row == 14) {
        abs_pmtx = 3;
        abs_pmty = 1;
    }
    if (mpmt_col == 0 && mpmt_row == 15) {
        abs_pmtx = 2;
        abs_pmty = 0;
    }
    if (mpmt_col == 1 && mpmt_row == 15) {
        abs_pmtx = 3;
        abs_pmty = 0;
    }

    //EC9
    if (mpmt_col == 0 && mpmt_row == 16) {
        abs_pmtx = 4;
        abs_pmty = 1;
    }
    if (mpmt_col == 1 && mpmt_row == 16) {
        abs_pmtx = 5;
        abs_pmty = 1;
    }
    if (mpmt_col == 0 && mpmt_row == 17) {
        abs_pmtx = 4;
        abs_pmty = 0;
    }
    if (mpmt_col == 1 && mpmt_row == 17) {
        abs_pmtx = 5;
        abs_pmty = 0;
    }

    Int_t abspixx = abs_pmtx * 8 + pix_x;
    Int_t abspixy = abs_pmty * 8 + (8 - pix_y); //Top-to-bottom

    return abspixy * 100 + abspixx;
}

Int_t Visualization::TrigVis7(TString fn, TString trgroot, TString outsuffix){
    //Trigger!!! EC 1 PMT 0 0 ; Row 1 Col 1 ;  GTU 168 Packet 1 , GTU internal 40 , Sum 26  Threshold 13 , Persistency 2
    std::ifstream in;
    TString dum;
    Int_t pmt_row,pmt_col,box_x,box_y,ptt_gtu,ptt_pck,ptt_intgtu,ptt_sum;
    Int_t n,i,k;

    TFile *fsimu = new TFile(fn);
    TTree *etree = (TTree*)fsimu->Get("tevent");
    UChar_t data[1][1][48][48];

    if(!etree || etree->IsZombie()) {
      throw std::runtime_error(TString::Format("File %s (fn) does not contain \"tevent\" tree.", fn.Data()).Data());
    }
    
    etree->SetBranchAddress("photon_count_data",data);

    gStyle->SetOptStat(0);

    n = 0;

    TCanvas *c1 = new TCanvas("c1","",1500,500);
    c1->Draw();
    c1->Divide(3,1);

    for(i=0; i<3; i++){
        c1->cd(i)->SetMargin(0.05,0.2,0.05,0.2);
        c1->cd(i)->SetLogz();
    }

    double PixelDimension[61];

    for(i=0; i<=60; i++) {
        PixelDimension[i] = (i/10)*27.5-82.5+(i%10!=0)*(2.23+2.88*((i-1)%10));
    }

    TH2I *snapgtu = new TH2I("snapgtu","",60,PixelDimension,60,PixelDimension);

    TH2I *framesnap2 = new TH2I("framesnap2","",60,PixelDimension,60,PixelDimension);

    TString ht = "@@@";
    TString fnames = trgroot;
    std::cout << fnames << std::endl;

    TFile *froot = new TFile(fnames);
    TTree *l1trg = (TTree*)froot->Get("l1trg");
    TTree *gtusry = (TTree*)froot->Get("gtusry");

    if(!l1trg || l1trg->IsZombie()) {
      throw std::runtime_error(TString::Format("File %s (fn) does not contain \"l1trg\" tree.", fn.Data()).Data());
    }

    if(!gtusry || gtusry->IsZombie()) {
      throw std::runtime_error(TString::Format("File %s (fn) does not contain \"gtusry\" tree.", fn.Data()).Data());
    }
    
    Int_t ecID,pmtRow,pmtCol,pixRow,pixCol,gtuGlobal,packetID,gtuInPacket,sumL1,thrL1,persistL1;

    l1trg->SetBranchAddress("ecID",&ecID);
    l1trg->SetBranchAddress("pmtRow",&pmtRow);
    l1trg->SetBranchAddress("pmtCol",&pmtCol);
    l1trg->SetBranchAddress("pixRow",&pixRow);
    l1trg->SetBranchAddress("pixCol",&pixCol);
    l1trg->SetBranchAddress("gtuGlobal",&gtuGlobal);
    l1trg->SetBranchAddress("packetID",&packetID);
    l1trg->SetBranchAddress("gtuInPacket",&gtuInPacket);
    l1trg->SetBranchAddress("sumL1",&sumL1);
    l1trg->SetBranchAddress("thrL1",&thrL1);
    l1trg->SetBranchAddress("persistL1",&persistL1);

    Int_t trgBoxPerGTU;
    Int_t trgPMTPerGTU;
    Int_t trgECPerGTU;
    Int_t sumL1PMT[18][2];
    Int_t sumL1PDM;
    Int_t nPersist,gtuInPersist;

    gtusry->SetBranchAddress("gtuGlobal",&gtuGlobal);
    gtusry->SetBranchAddress("trgBoxPerGTU",&trgBoxPerGTU);
    gtusry->SetBranchAddress("trgPMTPerGTU",&trgPMTPerGTU);
    gtusry->SetBranchAddress("trgECPerGTU",&trgECPerGTU);
    gtusry->SetBranchAddress("sumL1PMT",sumL1PMT);
    gtusry->SetBranchAddress("sumL1PDM",&sumL1PDM);
    gtusry->SetBranchAddress("nPersist",&nPersist);
    gtusry->SetBranchAddress("gtuInPersist",&gtuInPersist);

    UInt_t ntriginGTU[MAXGTUINFILE];

    for(i=0; i<gtusry->GetEntries(); i++) {
        gtusry->GetEntry(i);
        ntriginGTU[i]=trgBoxPerGTU;
    }

    Int_t iii;
    UInt_t trigboxesinframe = 0;
    for(iii=0; iii < l1trg->GetEntries(); iii++) {
        l1trg->GetEntry(iii);

        pmt_row    = pmtRow;
        pmt_col    = pmtCol;
        box_x      = pixRow;
        box_y      = pixCol;
        ptt_gtu    = gtuGlobal;
        ptt_pck    = packetID;
        ptt_intgtu = gtuInPacket;
        ptt_sum    = sumL1;

        Int_t abs_pixx;
        Int_t abs_pixy;
        Int_t refpix = mario2pdm(pmt_col,pmt_row,box_x,box_y);

        abs_pixx = refpix/100;
        abs_pixy = refpix%100;

        snapgtu->SetTitle(ht);
        snapgtu->SetBinContent(pixcoordinateonmapbin(abs_pixx,abs_pixy),ptt_sum);

        trigboxesinframe++;

        if(trigboxesinframe == ntriginGTU[gtuGlobal]) {
            c1->cd(1)->SetMargin(0.05,0.2,0.05,0.2);
            c1->cd(1)->SetLogz();
            c1->cd(2)->SetMargin(0.05,0.2,0.05,0.2);
            c1->cd(2)->SetLogz();
            c1->cd(3)->SetMargin(0.05,0.2,0.05,0.2);
            c1->cd(3)->SetGridx();
            c1->cd(3)->SetGridy();
            c1->cd(3)->SetTickx();
            c1->cd(3)->SetTicky();
            c1->cd(1);

            ht = fn;
            TString ht2;
            ht2 = "GTU#: " + TString(Form("%6d",gtuGlobal)) + " Packet: " +  TString(Form("%5d",ptt_pck)) + " GTU_{pkt}: " +  TString(Form("%3d",ptt_intgtu));

            gtusry->GetEntry(ptt_gtu);
            TString ht3;
            ht3 = "#_{box}: " +  TString(Form("%4d",trgBoxPerGTU)) + " #_{PMT}: " +  TString(Form("%2d",trgPMTPerGTU)) + " Sum_{PDM}:  " +  TString(Form("%5d",sumL1PDM));

            drawtrgcentmap(1);

            Int_t intedat = outsuffix.First("trn_");

            TString outsuffixdate = "";
            for ( k = 0 ; k < 23 ; k++){
                outsuffixdate += outsuffix[intedat+4+k];
            }

            TLatex *tibh = new TLatex(-82,112,outsuffixdate);
            tibh->Draw();
            //delete tibh;

            TLatex *tibh2 = new TLatex(-82,100,ht2);
            tibh2->Draw();
            //delete tibh2;

            TLatex *tibh3 = new TLatex(-82,88,ht3);
            tibh3->Draw();
            //delete tibh3;

            snapgtu->SetMinimum(0.0999);
            snapgtu->SetMaximum(1000.);
            snapgtu->Draw("colzsame");

            TString img_name = outsuffix;

            img_name += "_gtu_" +  TString::Format("%06d",ptt_gtu);

            hist_count->Reset();
            etree->GetEntry(gtuGlobal);

            Int_t nhitpix=0;

            for(Int_t row=0; row<48; row++) {//writes PDM-wise format (Lech)
                for(Int_t col=0;col<48;col++){
                    framesnap->SetBinContent(col+1,row+1,(Int_t)data[0][0][col][row]);
                    framesnap2->SetBinContent(10*(col/8)+1+col%8+1,10*(row/8)+1+row%8+1,(Int_t)data[0][0][col][row]);

                    hist_count->Fill((Int_t)data[0][0][col][row]);
                    nhitpix+=((Int_t)data[0][0][col][row])>0;
                }
            }

            c1->cd(2);
            PDMTemplate->SetMaximum(20);
            PDMTemplate->SetMinimum(0.5);
            drawtrgcentmap(2);
            framesnap2->SetMaximum(20);
            framesnap2->SetMinimum(0.5);
            framesnap2->Draw("colzsame");

            hist_count->GetMean();

            TString pdminfo1,pdminfo2;
            pdminfo1 = "#sum n_{pix}= ";
            pdminfo1 += TString::Format("%6d", int(framesnap2->Integral(1, 60, 1, 60)));
            pdminfo1 += " #LT n #GT = ";
            pdminfo1 += TString::Format("%.3f", hist_count->GetMean());

            pdminfo2 = "";
            if(hist_count->GetMean() > 0) {
                pdminfo2 += " #sigma / #LT n #GT = ";
                pdminfo2 += Form("%.2f", 100 * hist_count->GetRMS() / hist_count->GetMean());
                pdminfo2 += "% ";
            }

            pdminfo2 += "#hit_{pix}= ";
            pdminfo2 += TString::Format("%4d", nhitpix);

            TLatex *kpo = new TLatex(-82.5,110,pdminfo1);
            kpo->Draw();
            //delete kpo;

            TLatex *kpo1 = new TLatex(-82.5,90,pdminfo2);
            kpo1->Draw();
            //delete kpo1;

            c1->cd(3);
            trgframe->Reset();
            trgframe->GetXaxis()->SetNdivisions(6);
            trgframe->GetYaxis()->SetNdivisions(6);
            trgframe->Draw("");

            TString persinfo;

            persinfo = "Trig.Pers: " +  TString::Format("%3d",nPersist);
            gtusry->GetEntry(ptt_gtu);
            if (nPersist > 1){
                persinfo += " ( " +  TString::Format("%3d",gtuInPersist+1) + " ) ";
            }


            Int_t mov[6][6];
            for(Int_t nni=0; nni<6; nni++) {
                mov[nni][0]=mov[nni][1]=mov[nni][2]=mov[nni][3]=mov[nni][4]=mov[nni][5]=0;
            }
            for(Int_t nnn=ptt_gtu-1;  nnn<=ptt_gtu+1; nnn++) {
                gtusry->GetEntry(nnn);

                for(Int_t kkk=0; kkk<18; kkk++) {
                    for(Int_t jjj=0; jjj<2; jjj++) {
                        Int_t trg_yes = sumL1PMT[kkk][jjj];

                        refpix = mario2pdm(jjj,kkk,3,3);
                        abs_pixx = refpix/100;
                        abs_pixy = refpix%100;

                        if(trg_yes > 0) {
                            std::cout << gtuGlobal << " " << sumL1PMT[kkk][jjj] << " "<< kkk << " " << jjj << trg_yes << " " << refpix << std::endl;
                            trgpmt_yes->SetBinContent(abs_pixy/8+1,6-abs_pixx/8,nnn-ptt_gtu+ptt_intgtu,(trg_yes>0));

                            if(nnn == ptt_gtu) {
                                std::cout << nnn << " anonono " << " " << abs_pixx << " " << abs_pixy << " " << ptt_gtu << std::endl;
                                trgpmt->SetBinContent(abs_pixy/8+1,abs_pixx/8+1,trg_yes>0);
                            }
                            if(trg_yes) {
                                if(nnn == ptt_gtu-1) mov[abs_pixy/8][abs_pixx/8] += 1;
                                if(nnn == ptt_gtu) mov[abs_pixy/8][abs_pixx/8] += 2;
                                if(nnn == ptt_gtu+1) mov[abs_pixy/8][abs_pixx/8] += 4;
                            }
                        }
                    }
                }
            }

            trgframe->Draw("");
            TLatex *tibh5 = new TLatex(0,6.8,ht2);
            tibh5->Draw();
            //delete tibh5;

            TLatex *persinf = new TLatex(0,6.4,persinfo);
            std::cout << "A " << persinfo << " " << nPersist << std::endl;

            gtusry->GetEntry(ptt_gtu);
            if(nPersist > 1) {
                persinf->SetTextColor(kRed);
            } else {
                persinf->SetTextColor(kBlack);
            }
            persinf->Draw();
            //delete persinf;

            TLatex *refleg;

            for(Int_t xi=0; xi<6; xi++) {
                for(Int_t yi=0; yi<6; yi++) {
                    squarebox[xi][yi] = new TGraph(5) ;

                    if(mov[xi][yi] > 0) {
                        squarebox[xi][yi]->SetPoint(0,xi,yi);
                        squarebox[xi][yi]->SetPoint(1,xi+1,yi);
                        squarebox[xi][yi]->SetPoint(2,xi+1,yi+1);
                        squarebox[xi][yi]->SetPoint(3,xi,yi+1);
                        squarebox[xi][yi]->SetPoint(4,xi,yi);
                        if(mov[xi][yi] == 0) {
                            continue;
                        }
                        if(mov[xi][yi] == 1) {
                            squarebox[xi][yi]->SetFillColor(kRed);
                            refleg = new TLatex(4.5,6.2,"t_{0}#minus1");
                            refleg->SetTextColor(kRed);
                            refleg->SetTextAngle(0);
                            refleg->SetTextAlign(22);
                            refleg->Draw();
                        }
                        if ( mov[xi][yi]  == 2 ) {
                            squarebox[xi][yi]->SetFillColor(kGreen);
                            refleg = new TLatex(6.2,6.2,"t_{0}");
                            refleg->SetTextColor(kGreen);
                            refleg->SetTextAlign(22);
                            refleg->SetTextAngle(0);
                            refleg->SetTextAngle(315);
                            refleg->Draw();
                        }

                        if(mov[xi][yi] == 4) {
                            squarebox[xi][yi]->SetFillColor(kBlue);
                            refleg = new TLatex(6.2,4.5,"t_{0}+1");
                            refleg->SetTextAlign(22);
                            refleg->SetTextAngle(0);
                            refleg->SetTextColor(kBlue);
                            refleg->Draw();
                        }

                        if(mov[xi][yi] == 3) {
                            squarebox[xi][yi]->SetFillColor(kCyan);
                            refleg = new TLatex(5.5,6.5,"t_{0}#minus1,t_{0}");
                            refleg->SetTextAngle(0);
                            refleg->SetTextColor(kCyan);
                            refleg->SetTextAlign(22);
                            refleg->Draw();
                        }

                        if(mov[xi][yi] == 5) {
                            squarebox[xi][yi]->SetFillColor(kMagenta);
                            refleg = new TLatex(6.5,6.5,"t_{0}#minus1,t_{0}+1");
                            refleg->SetTextAngle(315);
                            refleg->SetTextAlign(22);
                            refleg->SetTextColor(kMagenta);
                            refleg->Draw();
                        }
                        if(mov[xi][yi] == 6) {
                            squarebox[xi][yi]->SetFillColor(kYellow);
                            refleg = new TLatex(6.5,5.5,"t_{0},t_{0}+1");
                            refleg->SetTextAngle(0);
                            refleg->SetTextAlign(22);
                            refleg->SetTextColor(kYellow);
                            refleg->Draw();
                        }

                        if(mov[xi][yi] == 7 ) {
                            squarebox[xi][yi]->SetFillColor(kBlack);
                            refleg = new TLatex(6.8,6.8,"t_{0}#minus,t_{0},t_{0}+1");
                            refleg->SetTextAngle(315);
                            refleg->SetTextAlign(22);
                            refleg->SetTextColor(kBlack);
                            refleg->Draw();
                        }
                        squarebox[xi][yi]->Draw("fsame");
                    }
                }
            }

            //delete refleg;

            gtusry->GetEntry(ptt_gtu);
            if(nPersist > 1) {
                img_name += "_seq";
            }
            else {
                img_name += "_isl";
            }

            std::cout << "Saving image " << img_name.Data() << " (entry " << iii << ")" << std::endl;
            c1->SaveAs(img_name+".gif");
            n++;

            trigboxesinframe=0;
            ht = "@@@";
            trgpmt_yes->Reset();
            trgpmt->Reset();
            snapgtu->Reset();
        }

        if (iii % 1000 == 0) {
            std::cout << iii << " entries processed" << std::endl;
        }
    }

    std::cout << " OK " << std::endl;

    //delete snapgtu;
    delete framesnap2;
    delete froot;
    delete c1;
    delete fsimu;
    return 0;
}
