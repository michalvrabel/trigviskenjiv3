#ifndef TRIGVISKENJIV3_SRC_ITERATOR_HH_
#define TRIGVISKENJIV3_SRC_ITERATOR_HH_

#include <iostream>
#include <stdio.h>
#include <vector>
#include <algorithm>
#include <iterator>
#include <cassert>

#include "Rtypes.h"
#include "TFile.h"
#include "TTree.h"

template <typename T>
class PhotonCountDataIterator {
    friend class iterator;
    friend class const_iterator;

    public:
        using size_type = Int_t;

        class iterator {
            public:
                using self_type = iterator;
                using value_type = T;
                using reference = T&;
                using pointer = T*;
                using iterator_category = std::forward_iterator_tag;
                using difference_type = Int_t;
                iterator() {ptr_ = NULL; array_ = NULL;}
                iterator(pointer ptr, PhotonCountDataIterator *array) : ptr_(ptr), array_(array) { }
                ~iterator() {}
                self_type operator=(const self_type& other) { ptr_ = other.ptr_; return *this; }

                self_type operator++() {
                    ptr_++;
                    if(*this == array_->end()) {
                        pointer nextPointer = array_->ReadNextGTU();
                        if(nextPointer != NULL) {
                            ptr_ = nextPointer;
                        }
                    }
                    return *this;
                }

                self_type operator++(Int_t) {
                    self_type i = *this;
                    ptr_++;
                    if(*this == array_->end()) {
                        pointer nextPointer = array_->ReadNextGTU();
                        if(nextPointer != NULL) {
                            ptr_ = nextPointer;
                        }
                    }
                    return i;
                }

                reference operator*() { return *ptr_; }
                pointer operator->() { return ptr_; }
                bool operator==(const self_type& rhs) { return ptr_ == rhs.ptr_; }
                bool operator!=(const self_type& rhs) { return ptr_ != rhs.ptr_; }
            private:
                pointer ptr_;
                PhotonCountDataIterator *array_;
        };

        class const_iterator {
            public:
                using self_type = const_iterator;
                using value_type = T;
                using reference = T&;
                using pointer = T*;
                using iterator_category = std::forward_iterator_tag;
                using difference_type = Int_t;
                const_iterator() {ptr_ = NULL; array_ = NULL;}
                const_iterator(pointer ptr, PhotonCountDataIterator *array) : ptr_(ptr), array_(array) { }
                ~const_iterator() {}
                self_type operator=(const self_type& other) { ptr_ = other.ptr_; return *this; }

                self_type operator++() {
                    ptr_++;
                    if(*this == array_->cend()) {
                        pointer nextPointer = array_->ReadNextGTU();
                        if(nextPointer != NULL) {
                            ptr_ = nextPointer;
                        }
                    }
                    return *this;
                }

                self_type operator++(Int_t) {
                    self_type i = *this;
                    ptr_++;
                    if(*this == array_->cend()) {
                        pointer nextPointer = array_->ReadNextGTU();
                        if(nextPointer != NULL) {
                            ptr_ = nextPointer;
                        }
                    }
                    return i;
                }

                const value_type& operator*() { return *ptr_; }
                const value_type* operator->() { return ptr_; }
                bool operator==(const self_type& rhs) { return ptr_ == rhs.ptr_; }
                bool operator!=(const self_type& rhs) { return ptr_ != rhs.ptr_; }
            private:
                pointer ptr_;
                PhotonCountDataIterator *array_;
        };

        PhotonCountDataIterator(TFile *etosFile, Bool_t lechFormat) {
            textFile_ = NULL;
            lechFormat_ = lechFormat;
            etree_ = (TTree*)etosFile->Get("tevent");
            
            if(!etree_ || etree_->IsZombie()) {
                throw std::runtime_error(TString::Format("File (etosFile) does not contain \"tevent\" tree.").Data());
            }
            
            entriesMax_ = (Int_t)etree_->GetEntries();
            data_ = new T[size_];
            entriesActual_ = 0;
            
            ReadGTUFromEtosFile();
        }

        PhotonCountDataIterator(FILE *textFile) {
            textFile_ = textFile;
            lechFormat_ = false;
            etree_ = NULL;
            entriesMax_ = -1;
            entriesActual_ = -1;

            data_ = new T[size_];

            ReadGTUFromTextFile();
        }

        ~PhotonCountDataIterator() {
            if(data_!=NULL) {
                delete[] data_;
                data_= NULL;
            }
        }

        size_type size() const { return size_; }

        T& operator[](size_type index) {
            assert(index < size_);
            return data_[index];
        }

        const T& operator[](size_type index) const {
            assert(index < size_);
            return data_[index];
        }

        iterator begin() {
            return iterator(data_, this);
        }

        iterator end() {
            return iterator(data_ + size_, this);
        }

        const_iterator cbegin() {
            return const_iterator(data_, this);
        }

        const_iterator cend() {
            return const_iterator(data_ + size_, this);
        }

    private:
        T* data_;
        size_type size_ = 2304; // 1 GTU == 2304 pixel (48*48)

        //Etos FILE
        TTree *etree_;
        size_type entriesMax_;
        size_type entriesActual_;
        Bool_t lechFormat_;

        //Text FILE
        FILE *textFile_;

        T* ReadNextGTU() {
            if(textFile_ == NULL && etree_ != NULL) {
                //Etos FILE
                if(entriesActual_ < entriesMax_) {
                    ReadGTUFromEtosFile();
                    return data_;
                }
            } else {
                //Text FILE
                if(ReadGTUFromTextFile() == true) {
                    return data_;
                }
            }

            return NULL;
        }

        void ReadGTUFromEtosFile() {
            UChar_t data[1][1][48][48];
            T data_T[48][48];
            etree_->SetBranchAddress("photon_count_data",data);
            Int_t next = 0;
            etree_->GetEntry(entriesActual_);

            // rotate 90 degrees counter clockwise
            for(Int_t row=0;row<48;row++) { //writes PDM-wise format (Lech)
                for(Int_t col=0;col<48;col++) {
                    data_T[row][col]=(T) (data[0][0][col][47-row]);
                }
            }

            if(lechFormat_ == true) {
                for(Int_t row=0;row<48;row++) {//writes PDM-wise format (Lech)
                    for(Int_t col=0;col<48;col++) {
                        data_[next] = data_T[row][col];
                        next++;
                    }
                }
            } else {
                for(Int_t rowEC=0;rowEC<3;rowEC++) {//writes EC-wise format (Mario)
                    for(Int_t colEC=0;colEC<3;colEC++) {
                        for(Int_t row=0;row<16;row++) {
                            for(Int_t col=0;col<16;col++) {
                                data_[next] = data_T[rowEC*16+row][colEC*16+col];
                                next++;
                            }
                        }
                    }
                }
            }
            entriesActual_++;
        }

        Bool_t ReadGTUFromTextFile() {
            Int_t next = 0;
            T number;
            while(next != 2304) {
                if(fscanf(textFile_, "%d", &number) != EOF) {
                    data_[next] = number;
                } else {
                    return false;
                }
                next++;
            }

            return true;
        }
};

#endif /* TRIGVISKENJIV3_SRC_ITERATOR_HH_ */
