/*
 * Trigger.hh
 *
 *  Created on: Feb 25, 2018
 *      Author: Kristián Goliaš
 */

#ifndef TRIGVISKENJIV3_SRC_TRIGGER_HH_
#define TRIGVISKENJIV3_SRC_TRIGGER_HH_

#include "Rtypes.h"
#include "TString.h"
#include "TH2I.h"
#include "TH1I.h"
#include "TStyle.h"
#include "TCanvas.h"
#include "TColor.h"

#include "Iterator.hh"
#include "Options.hh"

const int kTRIGGER_INVERSEMEANSFRAME_SIZE = 48*48;
const int kTRIGGER_EC_ASC_PMT_ROWS_SIZE = 18;
const int kTRIGGER_EC_ASC_PMT_COLS_SIZE = 2;
const int kTRIGGER_CHOSENTHRESHOLDS_SIZE = 2;
const int kTRIGGER_TRIGGERTHRESHOLD_ROWS_SIZE = 100;  
// kTRIGGER_TRIGGERTHRESHOLD_ROWS_SIZE could be potentionally 63, but changes are required
const int kTRIGGER_TRIGGERTHRESHOLD_COLS_SIZE = 5;  
const int kTRIGGER_EC_ASC_ROWS_SIZE = 144;
const int kTRIGGER_EC_ASC_COLS_SIZE = 16;


class Trigger {
public:
    Trigger(const Options& options);
    virtual ~Trigger();
    
    void CreateHistogramAndGraphForMaxForPMT();
    void CreateHistogramForThresholds();
    void CreateHistogramForNumberOfFrame();
    void CreateHistogramForTriggerRate();
    void ReadThresholdsForFirstPacket(Float_t ThresholdLoweringFactor,
                                      Int_t packet_size,
                                      PhotonCountDataIterator<Int_t> *iterate);
    void FenuRootOut(Float_t ThresholdLoweringFactor, Int_t packet_size, 
                     PhotonCountDataIterator<Int_t> *iterate,
                     const char* outputrootfile, const char* fname_stdout_prefix="");
    void SetChosenTresholds(const Int_t row, const Int_t col, const Float_t &triggerBin);
    Int_t GetCountValidPMT();
        
private:
    const Options& fOptions;
    Double_t fGtuCounter;
    
    std::vector<Int_t> numberOfFrameForHistogram; //Visualization
    std::vector<Int_t> triggerRateForHistogram; //Visualization
    std::vector<Float_t> maxForPMTvalues[kTRIGGER_EC_ASC_PMT_ROWS_SIZE][kTRIGGER_EC_ASC_PMT_COLS_SIZE]; //Visualization
    
    Float_t thresholdsForPMT[kTRIGGER_EC_ASC_PMT_ROWS_SIZE][kTRIGGER_EC_ASC_PMT_COLS_SIZE];
    Float_t thresholdsForPMTGlobalGTU[kTRIGGER_EC_ASC_PMT_ROWS_SIZE][kTRIGGER_EC_ASC_PMT_COLS_SIZE];
    Bool_t validTresholdsForPMT[kTRIGGER_EC_ASC_PMT_ROWS_SIZE][kTRIGGER_EC_ASC_PMT_COLS_SIZE];
    UInt_t validMaxForPMT[kTRIGGER_EC_ASC_PMT_ROWS_SIZE][kTRIGGER_EC_ASC_PMT_COLS_SIZE]; //Visualization
    UInt_t invalidMaxForPMT[kTRIGGER_EC_ASC_PMT_ROWS_SIZE][kTRIGGER_EC_ASC_PMT_COLS_SIZE]; //Visualization
    Float_t minThresholdsForPMT[kTRIGGER_EC_ASC_PMT_ROWS_SIZE][kTRIGGER_EC_ASC_PMT_COLS_SIZE];
    Float_t maxThresholdsForPMT[kTRIGGER_EC_ASC_PMT_ROWS_SIZE][kTRIGGER_EC_ASC_PMT_COLS_SIZE];
    Int_t chosenThresholds[kTRIGGER_CHOSENTHRESHOLDS_SIZE][kTRIGGER_EC_ASC_PMT_ROWS_SIZE][kTRIGGER_EC_ASC_PMT_COLS_SIZE];
    Float_t triggerThresholds[kTRIGGER_TRIGGERTHRESHOLD_ROWS_SIZE][kTRIGGER_TRIGGERTHRESHOLD_COLS_SIZE];
    Float_t inverseMeansFrame[kTRIGGER_INVERSEMEANSFRAME_SIZE];
    Int_t maskPlot[kTRIGGER_EC_ASC_ROWS_SIZE][kTRIGGER_EC_ASC_COLS_SIZE];
    
    Double_t GetXUpValueForHistogram(Int_t maxValue, Int_t numberOfBean);
    
    std::string GetTime();
};

#endif /* TRIGVISKENJIV3_SRC_TRIGGER_HH_ */
