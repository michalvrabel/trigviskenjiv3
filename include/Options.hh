/*
 * Options.hh
 *
 *  Created on: Feb 4, 2018
 *      Author: Kristián Goliaš
 */

#ifndef TRIGVISKENJIV3_SRC_OPTION_HH_
#define TRIGVISKENJIV3_SRC_OPTION_HH_

#include <string>
#include <vector>

#include "Rtypes.h"
#include "TString.h"

class Options {
public:
    Options(Int_t argc, char **argv);
    virtual ~Options();
    
    static void PrintHelp();
    
    Bool_t IsConvertYes() const { return convertYes; }
    const TString& GetDataDir() const { return dataDir; }
    Bool_t IsOverwrite() const { return overwrite; }
    Bool_t IsDotrigger() const { return dotrigger; }
    Bool_t IsEcAscInput() const { return ecAscInput; }
    Int_t GetHelp() const { return help; }
    const std::vector<std::string>& GetInputFiles() const { return inputFiles; }
    const TString& GetMaskFilePathname() const { return maskFilePathname; }
    const TString& GetMaxSignalsFilePathname() const { return maxSignalsFilePathname; }
    const TString& GetMinSignalsFilePathname() const { return minSignalsFilePathname; }
    Int_t GetPacketSize() const { return packetSize; }
    Bool_t IsSimplePrefix() const { return simplePrefix; }
    const TString& GetThresholdsFilePathname() const { return thresholdsFilePathname; }
    Bool_t IsVisualisation() const { return visualisation; }
    Bool_t IsHelp() const { return help; }
    Bool_t IsReadThresholdsForFirstPacket() const { return readThresholdsForFirstPacket; }
    Bool_t IsUseMinAndMaxSignals() const { return useMinAndMaxSignals; }
    Float_t GetAveBgFactor() const { return ave_bg_factor; }
    Bool_t IsUseThresholdForPdm() const { return useThresholdForPDM; }
    Int_t GetMinNumValidPmt() const { return minNumValidPMT; }
    Bool_t IsTriggerVisual() const { return triggerVisual; }
    Bool_t IsInverseMeansFrame() const { return inverseMeansFrame; }
    const TString& GetInverseMeansFramePathname() const { return inverseMeansFramePathname; }
    Int_t GetMaxFileNameLength() const { return maxFileNameLength; }
private:
    std::vector<std::string> inputFiles;
    TString dataDir;
    TString thresholdsFilePathname;
    TString minSignalsFilePathname;
    TString maxSignalsFilePathname;
    TString maskFilePathname;
    TString inverseMeansFramePathname;
    Bool_t help;
    Bool_t overwrite;
    Bool_t visualisation;
    Bool_t convertYes;
    Bool_t dotrigger;
    Bool_t simplePrefix;
    Bool_t ecAscInput;
    Bool_t readThresholdsForFirstPacket;
    Bool_t useMinAndMaxSignals;
    Bool_t useThresholdForPDM;
    Bool_t triggerVisual;
    Bool_t inverseMeansFrame;
    Int_t packetSize;
    Float_t ave_bg_factor;
    Int_t minNumValidPMT;
    Int_t maxFileNameLength;
protected:    
    Bool_t ArgIsTrue(const char* optarg_v) const;
    Bool_t ArgIsFalse(const char* optarg_v) const;
};

#endif /* TRIGVISKENJIV3_SRC_OPTION_HH_ */
