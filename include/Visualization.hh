/*
 * Visualization.hh
 *
 *  Created on: Feb 18, 2018
 *      Author: Kristián Goliaš
 */

#ifndef TRIGVISKENJIV3_SRC_VISUALIZATION_HH_
#define TRIGVISKENJIV3_SRC_VISUALIZATION_HH_

#include <iostream>
#include <fstream>

#include "TH2I.h"
#include "TH2D.h"
#include "TH3I.h"
#include "TGraph.h"
#include "Rtypes.h"
#include "TFile.h"
#include "TTree.h"
#include "TCanvas.h"
#include "TLatex.h"
#include "TStyle.h"

#include "Palette.hh"

class Visualization {
public:
    Visualization();
    virtual ~Visualization();
    Int_t TrigVis7(TString fn, TString trgroot, TString outsuffix);
private:
    const TH2I *pmtandgapmatrix;
    TH2D *PDMFrame;
    TH2D *PDMTemplate;
    TH2D *PDMGap;
    TH2I *framesnap;
    TH1I *hist_count;
    TH2I *trgframe;
    TH2I *trgpmt;
    TH2I *trgec;
    TGraph *squarebox[6][6];
    TGraph *squareboxref;
    TH3I *trgpmt_yes;

    Int_t pixcoordinateonmapbin(Int_t abspixx, Int_t abspixy);
    Int_t mesher(Int_t type);
    Int_t drawtrgcentmap(Int_t type);
    Int_t mario2pdm(Int_t mpmt_col, Int_t mpmt_row, Int_t pix_x, Int_t pix_y);
};

#endif /* TRIGVISKENJIV3_SRC_VISUALIZATION_HH_ */
