/*
 * Conversion.hh
 *
 *  Created on: Feb 18, 2018
 *      Author: Kristián Goliaš
 */

#ifndef TRIGVISKENJIV3_SRC_CONVERSION_HH_
#define TRIGVISKENJIV3_SRC_CONVERSION_HH_

#include <iostream>
#include <utility>

#include "TString.h"
#include "TFile.h"
#include "TTree.h"

class Conversion {
public:
    Conversion();
    virtual ~Conversion();
    Int_t ConvertFile3(const char* simuout, const char* pdm_asc_pathname, const char* ec_asc_pathname);
};

#endif /* TRIGVISKENJIV3_SRC_CONVERSION_HH_ */
