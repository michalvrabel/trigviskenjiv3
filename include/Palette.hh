/*
 * Palette.hh
 *
 *  Created on: Feb 4, 2018
 *      Author: Kristián Goliaš
 */

#ifndef TRIGVISKENJIV3_SRC_PALETTE_HH_
#define TRIGVISKENJIV3_SRC_PALETTE_HH_

#include "Rtypes.h"
#include "TColor.h"
#include "TStyle.h"

class Palette {
public:
    Palette();
    Int_t BPalette();
    Int_t InvBPalette();
    virtual ~Palette();
};

#endif /* TRIGVISKENJIV3_SRC_PALETTE_HH_ */
